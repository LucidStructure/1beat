<script>
	// function white() { 
	// 	$('.hdr-wrp').addClass('white') 
	// }
	// <?php $p = isset($_GET['p']) ? $_GET['p'] : FALSE;
	// 	if ($p == 'media'){
	// 		echo 'white()';
	// 	}
	// ?>
</script>
<div class="content music">
	<h2>2012 MIXTAPE</h2>
	<div class="mx-12"></div>
	<p>The OneBeat 2012 Mixtape distills the entire month of collaboration during OneBeat 2012, highlighting Fellows' mobile studio sessions, 
	recordings from Honey Jar and Studio G in New York City, workshops with youth at Jefferson Center's Music Lab and the YMCA in Charleston, 
	street studios in Roanoke, VA, and recordings from the Atlantic Center for the Arts in Florida.</p>
	<p>During the residency, the fellows formed improvised ensembles that reinvented and arranged traditional tunes, 
	composed original work, and recorded in the state-of-the-art OneBeat mobile studio. Following the residency, while on tour, 
	the Fellows also collaborated with local artists, designed free public music-making events, and led workshops and recording 
	sessions with youth. Featuring innovative young musicians from around the world, the 2012 OneBeat Mixtape reveals a powerful 
	vision of "world music" and illustrates the versatility and musical diversity of the fellows.</p>
	<iframe width="100%" height="900" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/8697052&amp;color=000000&amp;auto_play=false&amp;hide_related=true&amp;show_artwork=true"></iframe> 
	<em>artwork by <a href="http://www.lawsonworks.com/">John K Lawson</a> and <a href="http://hannahdevereux.co.uk/">Hannah Devereux</a></em> 
	<ol class="tracklist">
		<h2>Tracklist</h2> 
		<li class="track">
			<strong>Maqam Rashdi/Talaa Min Beet Abuha</strong> 
			<a href="/about/fellows/p/#chancemccoy">Chance McCoy</a> (US), 
			<a href="/about/fellows/p/#aditibhagwat">Aditi Bhagwat</a> (India), 
			<a href="/about/fellows/p/#amirelsaffar">Amir ElSaffar</a> (US), 
			<a href="/about/fellows/p/#kyungsopark">Kyungso Park</a> (South Korea), 
			<a href="/about/fellows/p/#devingreenwood">Devin Greenwood</a> (USA) 
		</li>
		<li class="track">
			<strong>Dreamer</strong> 
			<a href="/about/fellows/p/#antonsergeev">Anton Sergeev</a> (Russia), 
			<a href="/about/fellows/p/#chancemccoy">Chance McCoy</a> (US), 
			<a href="/about/fellows/p/#EdwardRamirez">Edward Ramirez</a> (Venezuela), 
			<a href="/about/fellows/p/#AymanMabrouk">Ayman Mabrouk</a> (Egypt), 
			<a href="/about/fellows/p/#ninaogot">Nina Ogot</a> (Kenya), 
			<a href="/about/fellows/p/#sidseholte">Sidse Holte</a> (Denmark), 
			<a href="/about/fellows/p/#domenicafossati">Domenica Fossati</a> (US)
		</li>
		<li class="track">
			<strong>Gumbri</strong> <a href="/about/fellows/p/#waeljegham">Wael Jegham</a> (Tunisia), 
			<a href="/about/fellows/p/#alesh">Alesh</a> (DRC), 
			<a href="/about/fellows/p/#pps">PPS</a> (Senegal), 
			<a href="/about/fellows/p/#johnbernthomas">Johnbern Thomas</a> (Haiti), 
			<a href="/about/fellows/p/#christophermarianetti">Chris Marianetti</a> (US) 
		</li>
		<li class="track">
			<strong>Iba</strong> <a href="/about/fellows/p/#katochange">Kato Change</a> (Kenya), 
			<a href="/about/fellows/p/#kayodekuti">Kayode Kuti</a> (Nigeria), 
			<a href="/about/fellows/p/#parfumzola">Parfum Zola</a> (DRC) 
		</li>
		<li class="track">
			<strong>In The Hills</strong> <a href="/about/fellows/p/#piotrkurek">Piotr Kurek</a> (Poland), 
			<a href="/about/fellows/p/#songheekwon">Song Hee Kwon</a> (South Korea), 
			<a href="/about/fellows/p/#youmnasaba">Youmna Saba</a> (Lebanon), 
			<a href="/about/fellows/p/#weronikapartyka">Weronika Partyka</a> (Poland), 
			<a href="/about/fellows/p/#srijokoraharjo">Sri Joko Raharjo</a> (Indonesia), 
			<a href="/about/fellows/p/#aditibhagwat">Aditi Bhagwat</a> (India) 
		</li>
		<li class="track">
			<strong>This Time</strong> <a href="/about/fellows/p/#edwardramirez">Edward Ramirez</a> (Venezuela), 
			<a href="/about/fellows/p/#verajonas">Vera Jonas</a> (Hungary), 
			<a href="/about/fellows/p/#devingreenwood">Devin Greenwood</a> (USA), 
			<a href="/about/fellows/p/#sidseholte">Sidse Holte</a> (Denmark), 
			<a href="/about/fellows/p/#chancemccoy">Chance McCoy</a> (US), Denison Witmer (US), 
			<a href="/about/fellows/p/#gideoncrevoshay">Gideon Crevoshay</a> (US) 
		</li>
		<li class="track">
			<strong>Gone Gone</strong> <a href="/about/fellows/p/#waeljegham">Wael Jegham</a> (Tunisia), 
			<a href="/about/fellows/p/#aquilcharlton">Aquil Charlton</a> (US) 
		</li>
		<li class="track">
			<strong>Hey!</strong> <a href="/about/fellows/p/#pps">PPS</a> (Senegal), 
			<a href="/about/fellows/p/#alesh">Alesh</a> (DRC), 
			<a href="/about/fellows/p/#jeremythal">Jeremy Thal </a>(USA), YMCA Kids 
		</li>
		<li class="track">
			<strong>My Hometown Spring</strong> <a href="/about/fellows/p/#chancemccoy">Chance McCoy</a> (US), 
			<a href="/about/fellows/p/#aditibhagwat">Aditi Bhagwat</a> (India), 
			<a href="/about/fellows/p/#amirelsaffar">Amir ElSaffar</a> (US), 
			<a href="/about/fellows/p/#kyungsopark">Kyungso Park</a> (South Korea) 
		</li>
		<li class="track">
			<strong>A Town With No Sound</strong> <a href="/about/fellows/p/#ceasark">Ceasar K</a> (Lebanon), 
			<a href="/about/fellows/p/#pps">PPS</a> (Senegal), 
			<a href="/about/fellows/p/#ninaogot">Nina Ogot</a> (Kenya), 
			<a href="/about/fellows/p/#AymanMabrouk">Ayman Mabrouk</a> (Egypt)
		</li>
		<li class="track">John K Lawson (USA) 
			<strong>Pillar of Cloud</strong> 
			<a href="/about/fellows/p/#youmnasaba">Youmna Saba</a> (Lebanon), 
			<a href="/about/fellows/p/#verajonas">Vera Jonas</a> (Hungary), 
			<a href="/about/fellows/p/#amirelsaffar">Amir ElSaffar</a> (US), 
			<a href="/about/fellows/p/#samitasinha">Samita Sinha</a> (US), 
			<a href="/about/fellows/p/#srijokoraharjo">Sri Joko Raharjo</a> (Indonesia), 
			<a href="/about/fellows/p/#piotrkurek">Piotr Kurek</a> (Poland), 
			<a href="/about/fellows/p/#domenicafossati">Domenica Fossati</a> (US) 
		</li>
		<li class="track">
			<strong>Grandfather Mountain</strong> 
			<a href="/about/fellows/p/#sidseholte">Sidse Holte</a> (Denmark), 
			<a href="/about/fellows/p/#chancemccoy">Chance McCoy</a> (US) 
		</li>
		<li class="track">
			<strong>Slow Descent</strong> 
			<a href="http://foundsoundnation.org/people/antonsergeev/">Anton Sergeev</a> (Russia)
			<a href="http://foundsoundnation.org/people/usmanriaz/">Usman Riaz</a> (Pakistan) 
		</li>
		<li class="track">
			<strong>Jano Janke</strong> 
			<a href="http://foundsoundnation.org/people/evasalinaprimack/">Eva Salina Primack</a> (US) 
		</li>
		<li class="track">
			<strong>Come On</strong> <a href="http://foundsoundnation.org/people/antonsergeev/">Anton Sergeev</a> (Russia), 
			<a href="http://foundsoundnation.org/people/EdwardRamirez/">Edward Ramirez</a> (Venezuela), 
			<a href="http://foundsoundnation.org/people/chancemccoy/">Chance McCoy</a> (US), 
			<a href="http://foundsoundnation.org/people/AymanMabrouk/">Ayman Mabrouk</a> (Egypt), 
			<a href="http://foundsoundnation.org/people/domenicafossati/">Domenica Fossati</a> (US), 
			<a href="http://foundsoundnation.org/people/devingreenwood/">Devin Greenwood</a> (US), 
			<a href="http://foundsoundnation.org/people/evasalinaprimack/">Eva Salina Primack</a> (US) 
		</li>
		<li class="track">
			<strong>Alone When It Happened</strong> <a href="http://foundsoundnation.org/people/weiwei/">Wei Wei</a> (China), 
			<a href="http://foundsoundnation.org/people/piotrkurek/">Piotr Kurek</a> (Poland) 
		</li>
		<li class="track">
			<strong>Reimagination</strong> <a href="http://foundsoundnation.org/people/jessicasrin/">Jessica "Lisha" Srin</a> (Cambodia), 
			<a href="http://foundsoundnation.org/people/devingreenwood/">Devin Greenwood</a> (US) 
		</li>
		<li class="track">
			<strong>Big Bang</strong> 
			<a href="http://foundsoundnation.org/people/sayakbarua/">Sayak Barua</a> (India)
		</li> 
		<li class="track">
			<strong>Silly Bird</strong> 
			<a href="http://foundsoundnation.org/people/ninaogot/">Nina Ogot</a> (Kenya), 
			<a href="http://foundsoundnation.org/people/chancemccoy/">Chance McCoy</a> (US), 
			<a href="http://foundsoundnation.org/people/EdwardRamirez/">Edward Ramirez</a> (Venezuela), 
			<a href="http://foundsoundnation.org/people/AymanMabrouk/">Ayman Mabrouk</a> (Egypt), 
			<a href="http://foundsoundnation.org/people/sidseholte/">Sidse Holte</a> (Denmark), 
			<a href="http://foundsoundnation.org/people/domenicafossati/">Domenica Fossati</a> (US), 
			<a href="http://foundsoundnation.org/people/johnbernthomas/">Johnbern Thomas</a> (Haiti) 
		</li>
		<li class="track">
			<strong>Dollar and a Dream</strong> <a href="http://foundsoundnation.org/people/muthonindonga/">Muthoni Ndonga</a> (Kenya), 
			<a href="http://foundsoundnation.org/people/ricardonigaglioni/">Ricardo Nigaglioni</a> (US), 
			<a href="http://foundsoundnation.org/people/pps/">PPS</a> (Senegal), 
			<a href="http://foundsoundnation.org/people/christophermarianetti/">Chris Marianetti</a> (US), 
			<a href="http://foundsoundnation.org/people/katochange/">Kato Change</a> (Kenya), 
			<a href="http://foundsoundnation.org/people/heliovanimal/">Helio Vanimal</a> (Mozambique), 
			<a href="http://foundsoundnation.org/people/elenamoonpark/">Elena Moon Park </a>(US) 
		</li>
		<li class="track">
			<strong>Life</strong> <a href="http://foundsoundnation.org/people/antonsergeev/">Anton Sergeev</a> (Russia), 
			<a href="http://foundsoundnation.org/people/chancemccoy/">Chance McCoy</a> (US), 
			<a href="http://foundsoundnation.org/people/EdwardRamirez/">Edward Ramirez</a> (Venezuela), 
			<a href="http://foundsoundnation.org/people/AymanMabrouk/">Ayman Mabrouk</a> (Egypt), 
			<a href="http://foundsoundnation.org/people/ninaogot/">Nina Ogot</a> (Kenya), 
			<a href="http://foundsoundnation.org/people/domenicafossati/">Domenica Fossati</a> (US)
		</li>
	</div>
</div>

