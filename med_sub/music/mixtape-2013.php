<div class="content mixtape-2013">
	<div class="listen cf">
		<h2>MIXTAPE 2013</h2>
		<a href="https://soundcloud.com/found-sound-nation/sets/onebeat-2013-mixtape-mp3-preview" class="cover"></a>
		<ul class="mix">
			<li class="click"><span>click track to listen</span></li>
			<li class="love" name="love">Love</li>
			<li class="apsara" name="apsara">Apsara</li>
			<li class="raindrop" name="raindrop">Raindrop</li>
			<li class="habayit" name="habayit">Habayit Sheli</li>
			<li class="counting" name="counting">Counting Song</li>
			<li class="bad-form" name="bad-form">Bad Form</li>
			<li class="piazzola" name="piazzola">Piazolla</li>
			<li class="yer" name="yer">Y.E.R.</li>
			<li class="tapdance" name="tapdance">Tap Dance</li>
			<li class="vojak" name="vojak">Vojak</li>
			<li class="dugu" name="dugu">Dugu</li>
		</ul>
	</div>
	<p>"The OneBeat 2013 Mixtape is a sonic concoction of musical talents of completely different 
	cultural and ethnic backgrounds from around the world; it's what happened when we were thrown 
	into a free-spirited month-long adventure exploring our music, cultures and ideas. Though the 
	material is richly layered with aesthetics from various cultures, it manages to let all these 
	styles get lost within themselves (in a good way), making them seem like one.  It's packed with 
	traditional folk songs, new-age electronics, unorthodox sounds from a mobile street-studio and 
	original work recorded at some of the best studios in New York city.  This music stands as a 
	testimony that OneBeat is a collaboration with an extraordinary energy that transcends cultural 
	barriers, man-made boundaries, and language.</p>
	<p>This is not a compilation album of pre-conceived musical ideas but a collage of ideas and emotions that 
	we experienced during out time together and communicated as sounds. The diversity of these musicians coupled 
	with the spirit of togetherness we managed to create at OneBeat make for this compelling textures of sound. 
	I hope this will be a sonic adventure for you, just as it was for us when we created it." </p>	
	<p><a href="http://1beat.org/about/fellows/p/#DarbukaSiva">- Darbuka Siva</a> OneBeat 2013 Fellow, Chennai, India</p>
	<div class="support">
		<p><em>We are grateful for support from the Peluso Microphone Lab, who gave us access to their 
		incredible vintage mics for the duration of the OneBeat program, Glyph, who donated a number of 
		their industry-leading hard drives, and Ableton who gave us access to their game-changing software.</em></p>
		<div class="logos">
			<a href="http://www.pelusomicrophonelab.com/" class="logo peluso"></a>
			<a href="http://www.glyphtech.com/" class="logo glyph"></a>
			<a href="https://www.ableton.com/" class="logo ableton"></a>
		</div>
	</div>
	<div class="tape cf">
		
		<div class="tracks">
			<div class="track one" name="one">
				<h3>Love</h3>
				<iframe id="love" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168009%3Fsecret_token%3Ds-2PMgb&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#YCtheCynic">YC the Cynic,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#AhmedRock">Ahmed Rock,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#MalabikaBrahma">Malabika Brahma,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#YangFanLi">Yang Fan,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#JihaPark">Jiha Park,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#DevinGreenwood">Devin Greenwood</a></li>
					</ul>
				</div>
				<div class="quote">
					<p>"OneBeat has definitely opened my mind and ears to other types of music, instruments, tempos, beat counts, and everything. On a more professional level, I've learned how to create music with others, which I had never done before. All of the give and take, dialogue, and compromise was very new to me."</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#YCtheCynic">YC the Cynic</a>, Rapper</li>
						<li>Bronx, New York</li>
					</ul>
				</div>
			</div>
			<div class="track two" name="two">
				<h3>Apsara</h3>
				<iframe id="apsara" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168022%3Fsecret_token%3Ds-Aswwh&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#YornYoung">Young Yorn,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#RuthDanon">Ruth Danon,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#YangFanLi">Yang Fan,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Elena Moon Park">Elena Moon Park,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Jeremy Thal">Jeremy Thal</a></li>
					</ul>
				</div>
				<div class="quote">
					<p>"This song is based on a traditional Cambodian melody. Apsara is the name of a female spirit carved in the stone walls of many 
					ancient Khmer temples in Cambodia. Due to war and political crisis, many temples have been abandoned and destroyed by people 
					and natural causes. Some Apsaras have lost theirs heads or hands, and some of them lost their legs or faces, 
					but they are still smiling."</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#YornYoung">Young Yorn,</a> Vocalist</li>
						<li>Phnom Penh, Cambodia</li>
					</ul>
				</div>
			</div>
			<div class="track three" name="three">
				<h3>Raindrop</h3>
				<iframe id="raindrop" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168005%3Fsecret_token%3Ds-Xchnw&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#MityaBurmistrov">Mitya Burmistov,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#BlinkyBill">Blinky Bill,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#MalabikaBrahma">Malabika Brahma</a></li>
					</ul>
				</div>
				<div class="quote">
					<p>"My musical instincts always drive me to express what I hear in my mind. 
					But when you are in the same room as some of the best musicians from all over the globe, 
					you don't have time to think -- you just start doing."</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#MityaBurmistrov">	Mitya Burmistov</a>, Beatboxer & Producer</li>
						<li>Kazan, Russia</li>
					</ul>
				</div>
			</div>
			<div class="track four" name="four">
				<h3>Habayit Sheli</h3>
				<iframe id="habayit" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/159815582%3Fsecret_token%3Ds-UzjVC&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_artwork=false&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#RuthDanon">Ruth Danon,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#YangFanLi">Yang Fan,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Elena Moon Park">Elena Moon Park,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Jeremy Thal">Jeremy Thal</a></li>
					</ul>
				</div>
				<div class="quote">
					<p>"The OneBeat residency brought out a lot of thoughts about what is home, what is needed 
					to make a house into a home and what we need to thrive in the place where we live and come 
					from. I suppose this song is a sort of contemplation about that; some wishful thinking about 
					what an ideal home should be like."</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#RuthDanon">Ruth Danon,</a> Vocalist/Keyboard</li>
						<li>Tel Aviv, Israel</li>
					</ul>
				</div>
			</div>
			<div class="track five" name="five">
				<h3>Counting Song</h3>
				<iframe id="counting" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168017%3Fsecret_token%3Ds-HQOz6&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
					<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#DarbukaSiva">Darbuka Siva,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#AuroraNealand">Aurora Nealand,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#OmarAmado">Omar Amado,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#ManuelRangel">Manuel Rangel,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#BiodunKuti">Biodun Kuti,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Jeremy Thal">Jeremy Thal</a></li>
					</ul>
				</div>
				<div class="quote">
					<p>"This is a rendition of 'Oru kodam, 'a Tamil folk song from Tamil Nadu, 
					South India. Simply put, it's the Tamil equivalent of 'Ring around the Rosie'. 
					I've done lots of collaborations and cross cultural experiments, but what we did on 
					'Counting Song' sounds like a band coming together. It's not a mashup of one culture 
					atop of another culture. It's a culmination of different cultures and different sounds. 
					We were able to bring out our identity as artists while still finding common ground."</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="">Darbuka Siva,</a> Percussionist, Producer</li>
						<li>Chennai, India</li>
					</ul>
				</div>
			</div>
			<div class="track six" name="six">
				<h3>Bad Form</h3>
				<iframe id="bad-form" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168019%3Fsecret_token%3Ds-pEDU8&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#ElyseTabet">Elyse Tabet,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Nandi Plunkett">Nandi Plunkett,</a></li>
						<li class="z">Ziad Moukarzel</li>
					</ul>
				</div>
				<div class="quote">
					<p>"There's always a compromise you make when someone else wants to take control. 
					To be able to do your thing in that spontaneous way in which you make it, and have 
					someone else do their thing next to you, and there's still this kind of harmony, that's really rare."
					</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#ElyseTabet">Elyse Tabet,</a> Audio-Visual Artist</li>
						<li>Beirut, Lebanon</li>
					</ul>
				</div>
			</div>
			<div class="track seven" name="seven">
				<h3>Piazolla</h3>
				<iframe id="piazzola" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168006%3Fsecret_token%3Ds-xbc8r&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/staff/p/#Chris Marianetti">Christopher Marianetti,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#DimaElSayed">Dima El Sayeed,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#MalabikaBrahma">Malabika Brahma,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#RicardoNigaglioni">Ricky Nigaglioni,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#CameronSteele">Cameron Steele,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Miles Bridges">Miles Bridges,</a></li>
						<li><a href="">Nathan Koci,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#SarahAlden">Sarah Alden,</a></li>
					</ul>
				</div>
				<div class="quote">
					<p>"This mix comes from a series of Street Studios we did in Charleston, N.C. and Brooklyn, N.Y. during OneBeat. 
					I like to think of this as a sort of sonic collective unconscious of these places. Childhood songs, pop tune melodies, 
					and dreams all bubble up and then get mixed together with improvised music. The Street Studios brought OneBeat musicians 
					and folks along the tour together in an unexpected way."</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/staff/p/#Chris Marianetti">Christopher Marianetti, </a>Composer, Pianist</li>
						<li>Queens, US</li>
					</ul>
				</div>
			</div>
			<div class="track eight" name="eight">
				<h3>Y.E.R</h3>
				<iframe id="yer" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/159815586%3Fsecret_token%3Ds-GKbE2&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
			<div class="feat">
				<p>Featuring: </p>
				<ul>
					<li class="z">Room 320</li>
				</ul>
			</div> 
				<div class="quote">
					<p>"She was no scholar in geometry or aught else, but she felt intuitively that the bend and slant 
					of the way she went were somehow outside any other angles or bends she had ever known."</p>
					</div>
				<div class="quote-e">
					<ul>
						<li>C. L. Moore</li>
					</ul>
				</div>
			</div>
			<div class="track nine" name="nine">
				<h3>Tap Dance</h3>
				<iframe id="tapdance" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168001%3Fsecret_token%3Ds-xW0gz&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#AntonSergeev">Anton Maskeliade,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#YornYoung">Young Yorn,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#Toussa">Toussa,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#RuthDanon">Ruth Danon,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Jeremy Thal">Jeremy Thal,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Elena Moon Park">Elena Moon Park,</a></li>
						<li>Campers from Girls Rock Charleston</li>
					
					</ul>
				</div>
				<div class="quote">
					<p>"I was a Fellow in 2012. It was exciting to remix artists from this past year -- 
					I sampled talented musicians from United States, Cambodia, China, Senegal and Israel. 
					I think the result is an eclectic adventure of very young sounds and mature silence."</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#AntonSergeev">Anton Maskeliade</a>, Electronic Musician</li>
						<li>Moscow, Russia</li>
					</ul>
				</div>
			</div>
			<div class="track ten" name="ten">
				<h3>Vojak</h3>
				<iframe id="vojak" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168000%3Fsecret_token%3Ds-lFl3j&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#LuciePachova">Lucie P&aacute;chov&aacute;,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#OmarAmado">Omar Amado,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#GregChudzik">Greg Chudzik,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#MpumiMcata">Mpumi Mcata,</a></li>
						<li><a href="http://1beat.org/about/staff/p/#Chris Marianetti">Christopher Marianetti</a></li>
					</ul>
				</div>
				<div class="quote">
					<p>"This '9 beat' song combines Eastern European folk rhythms with a harmonic minor scale. 
					The lyrics in old Czech language tell the story of a young soldier that was sent to war just before 
					becoming a farmer (or peasant). It is sung as a "reproach" to his mother who had consented to sending him to war. 
					I think music attempts to resonate with these lyrics."</p>
				</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/fellows/p/#LuciePachova">Lucie P&aacute;chov&aacute;</a>, Multi-media Composer & Vocalist</li>
						<li>Mlad&aacute; Boleslav, Czech Republic</li>
					</ul>
				</div>
			</div>
			<div class="track eleven" name="eleven">
				<h3>Dugu</h3>
				<iframe id="dugu" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/153168012%3Fsecret_token%3Ds-eUYU9&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_artwork=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
				<div class="feat">
					<p>Featuring: </p>
					<ul>
						<li><a href="http://1beat.org/about/staff/p/#Jeremy Thal">Jeremy Thal,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#Toussa">Toussa,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#YornYoung">Young Yorn,</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#RuthDanon">Ruth Danon</a></li>
						<li><a href="http://1beat.org/about/fellows/p/#YangFanLi">Yang Fan</a></li>
					</ul>
				</div>
				<div class="quote">
					<p>"Dugu is similar to the word 'True' or 'Right On' in Wolof. As a song, 
					it's a bit like a bunch of lost people all singing about a crumbled Tower of 
					Babel, without any notion that they are singing about the same thing."</p>
					</div>
				<div class="quote-e">
					<ul>
						<li><a href="http://1beat.org/about/staff/p/#Jeremy Thal">Jeremy Thal</a>, Horn Player / Producer</li>
						<li>Brooklyn, NY</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="credits">
		<h2>Credits</h2>
		<div class="song">
			<h3>Love</h3>
			<ul>
				<li>Produced by <a href="http://1beat.org/about/staff/p/#DevinGreenwood">Devin Greenwood</a></li>
				<li>Recorded at the Honey Jar, Brooklyn, New York</li>
				<li>YC the Cynic - Vocals</li>
				<li>Ahmed Rock - Vocals</li>
				<li>Malabika Brahma - Vocals</li>
				<li>Yang Fan - Guitar</li>
				<li>Jiha Park - Piri</li>
			</ul>
		</div>
		<div class="song">
			<h3>Apsara</h3>
			<ul>
				<li>Recorded by <a href="http://www.leosidran.com/">Leo Sidran</a> at Let 'Em In Studio, Brooklyn</li>
				<li>Young Yorn - Vocals</li>
				<li>Yang Fan - Guitar</li>
				<li>Ruth Danon - Flute</li>
				<li>Elena Moon Park - Violin</li>
				<li>Jeremy Thal - French Horn</li>
			</ul>
		</div>
		<div class="song">
			<h3>Raindrop</h3>
			<ul>
				<li>Produced by <a href="http://1beat.org/about/fellows/p/#MityaBurmistrov">Mitya Burmistov</a></li>
				<li>Recorded at Atlantic Center for the Arts, New Smyrna Beach</li>
				<li>Blinky Bill - Vocals</li>
				<li>Malabika Brahma - Vocals</li>
			</ul>
		</div>
		<div class="song">
			<h3>Habayit Sheli</h3>
			<ul>
				<li>Recorded by <a href="http://www.leosidran.com">Leo Sidran</a> at Let 'Em in Studio, Brooklyn</li>
				<li>Additional recording by Yang Fan in Beijing, China</li>
				<li>Ruth Danon - Vocals, Keyboard</li>
				<li>Yang Fan - Guitar</li>
				<li>Elena Moon Park - Violin</li>
				<li>Jeremy Thal - French Horn</li>
			</ul>
		</div>
		<div class="song">
			<h3>Counting Song</h3>
			<ul>
				<li>Recorded by Arun Pandian at <a href="https://www.facebook.com/pages/Superfund-Studio/115738205116123">Superfund Studio, Brooklyn</a></li>
				<li>'Darbuka' Siva - Vocals, Bass</li>
				<li>Aurora Nealand - Sax, Backing vocals</li>
				<li>Jeremy Thal - Trumpet, Backing vocals</li>
				<li>Biodun Kuti - Electric guitar</li>
				<li>Blinky Bill - Electric Guitar, Backing vocals</li>
				<li>Manuel Rangel - Maracas</li>
				<li>Omar Amado - Drums</li>
			</ul>
		</div>
		<div class="song">
			<h3>Bad Form</h3>
			<ul>
				<li>Recorded and produced by <a href="http://1beat.org/about/fellows/p/#ElyseTabet">Elyse Tabet</a> on her laptop, Beirut</li>
				<li>Mixed by Ziad Moukarzel in Qatar</li>
				<li>Nandi Plunkett - Vocals</li>
			</ul>
		</div>
		<div class="song">
			<h3>Piazolla</h3>
			<ul>
				<li>Produced by <a href="http://1beat.org/about/staff/p/#Chris%20Marianetti">Chris Marianetti</a> at Street Studios in Charleston, N.C & NYC</li>
				<li>Features samples of Sarah Alden, Darbuka Siva, Mitya Burmistov and passersby</li>
			</ul>
		</div>
		<div class="song">
			<h3>Y.E.R.</h3>
			<ul>
				<li>Produced by <a href="http://1beat.org/about/fellows/p/#YangFanLi">Yang Fan</a> at FSN Studio, Brooklyn</li>
				<li>Music by Room 320</li>
			</ul>
		</div>
		<div class="song">
			<h3>Tap Dance</h3>
			<ul>
				<li>Produced by <a href="http://1beat.org/about/fellows/p/#AntonSergeev">Anton Maskeliade</a> on his laptop in Moscow</li>
				<li>Features samples of Young Yorn, Toussa, Ruth Danon, Jeremy Thal, Elena Moon Park and campers from Girls Rock Charleston</li>
			</ul>
		</div>
		<div class="song">
			<h3>Vojak</h3>
			<ul>
				<li>Recorded by <a href="http://www.leosidran.com/">Leo Sidran</a> at Let 'Em In Studio, Brooklyn</li>
				<li>Lucie P&aacute;chov&aacute; - Vocals, Looping</li>
				<li>Omar Amado - Drums</li>
				<li>Greg Chudzik - Bass</li>
				<li>Mpumi Mcata - Guitar</li>
				<li>Christopher Marianetti - Piano</li>
			</ul>
		</div>
		<div class="song">
			<h3>Dugu</h3>
			<ul>
				<li>Produced by <a href="http://1beat.org/about/staff/p/#Jeremy%20Thal">Jeremy Thal</a></li>
				<li>Recorded at the Atlantic Center for the Arts, New Smyrna Beach</li>
				<li>Toussa - Vocals</li>
				<li>Young Yorn - Vocals, Yike Drum, Trou Sou</li>
				<li>Ruth Danon - Vocals</li>
				<li>Yang Fan - Guitar</li>
			</ul>
		</div>
		<h4>All songs mastered by <a href="http://1beat.org/about/staff/p/#DevinGreenwood">Devin Greenwood</a> and <a href="http://1beat.org/about/staff/p/#Ezra Tenenbaum">Ezra Tenenbaum</a></h4>
	</div>
	<div class="videos">
		<iframe width="560" height="315" src="//www.youtube.com/embed/as4yq5-14b4" frameborder="0" allowfullscreen></iframe>	
		<iframe width="560" height="315" src="//www.youtube.com/embed/8vahMongaSM" frameborder="0" allowfullscreen></iframe>
	</div>
</div>
<script type="text/javascript">
	$('ul.mix li').click(function() {
		var name = $(this).attr('name')
		var widgetIframe = document.getElementById(name)
	    var widget = SC.Widget(widgetIframe)
		widget.play()

		//navigation links 
		var a = $('ul.mix li.active')
		a.removeClass('active')
		$('ul.mix li[name='+name+']').addClass('active')
		$('html, body').animate({
	        scrollTop: $("#"+name+"").offset().top - 120
	    }, 400);
		
		//play next
	    widget.bind(SC.Widget.Events.FINISH, function() {	
	     	autoPlay(widgetIframe);
	    });

	    function autoPlay(widgetIframe) {
	    	var parent = widgetIframe.parentNode;
			var next = $(parent).next()
			var widgetNext = $(next).find( "iframe" )
			var nextID = ($(widgetNext).attr('id'))
			widgetIframe = document.getElementById(nextID)
			widgetNext = document.getElementById(nextID)
	      	widgetNext = SC.Widget(widgetNext)
	     	widgetNext.play()
	     	widgetNext.bind(SC.Widget.Events.FINISH, function() {	
	     		autoPlay(widgetIframe)
	     	});
	     	//navigation links 
			var a = $('ul.mix li.active')
			a.removeClass('active')
			a.next().addClass('active')
		}
	});	
</script>