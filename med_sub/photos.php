<script>
	// function white() { 
	// 	$('.hdr-wrp').addClass('white') 
	// }
	// <?php $p = isset($_GET['p']) ? $_GET['p'] : FALSE;
	// 	if ($p == 'media'){
	// 		echo 'white()';
	// 	}
	// ?>
</script>

<div class="content photos">

<!-- 	<h2>Photos</h2> -->
	<div id="photos" class="off">
		<div class="bxs-wrp cf" name="photos">
			<div class="bx l" id="o"></div>
			<div class="bx" id="t"></div>
			<div class="bx" id="f"></div>
			<div class="bx l" id="one"></div>
			<div class="bx" id="two"></div>
			<div class="bx" id="three"></div>
			<div class="bx l" id="four"></div>
			<div class="bx" id="five"></div>
			<div class="bx" id="six"></div>
			<div class="bx l" id="seven"></div>
			<div class="bx" id="eight"></div>
			<div class="bx" id="nine"></div>
			<div class="bx l" id="ten"></div>
			<div class="bx" id="eleven"></div>
			<div class="bx" id="twelve"></div>
			<div class="bx l" id="thirteen"></div>
			<div class="bx" id="fourteen"></div>
			<div class="bx" id="fifteen"></div>
			<div class="bx l" id="sixteen"></div>
			<div class="bx" id="seventeen"></div>
			<div class="bx" id="eighteen"></div>
			<div class="bx l" id="nineteen"></div>
			<div class="bx" id="twenty"></div>
			<div class="bx" id="twenty-one"></div>
			<div class="bx l" id="twenty-two"></div>
			<div class="bx" id="twenty-three"></div>
			<div class="bx" id="twenty-four"></div>
			<div class="bx l" id="twenty-five"></div>
			<div class="bx" id="twenty-six"></div>
			<div class="bx" id="twenty-seven"></div>
			<div class="bx l" id="twenty-eight"></div>
			<div class="bx" id="twenty-nine"></div>
			<div class="bx" id="thirty"></div>
		</div> 
	</div>
	
</div>
<div class="photo-slides">
	<div class="slide o off first"></div>
	<div class="slide t off"></div>
	<div class="slide th off"></div>
	<div class="slide f off"></div>
	<div class="slide one off"></div>
	<div class="slide two off"></div>
	<div class="slide three off"></div>
	<div class="slide four off"></div>
	<div class="slide five off"></div>
	<div class="slide six off"></div>
	<div class="slide seven off"></div>
	<div class="slide eight off"></div>
	<div class="slide nine off"></div>
	<div class="slide ten off"></div>
	<div class="slide eleven off"></div>
	<div class="slide twelve off"></div>
	<div class="slide thirteen off"></div>
	<div class="slide fourteen off"></div>
	<div class="slide fifteen off"></div>
	<div class="slide sixteen off"></div>
	<div class="slide seventeen off"></div>
	<div class="slide eighteen off"></div>
	<div class="slide nineteen off"></div>
	<div class="slide twenty off"></div>
	<div class="slide twenty-one off"></div>
	<div class="slide twenty-two off"></div>
	<div class="slide twenty-three off"></div>
	<div class="slide twenty-four off"></div>
	<div class="slide twenty-five off"></div>
	<div class="slide twenty-six off"></div>
	<div class="slide twenty-seven off"></div>
	<div class="slide twenty-eight off"></div>
	<div class="slide twenty-nine off"></div>
	<div class="slide thirty off last"></div>
</div>	
<div class="arws-wrap">
	<div class="arws">
		<div class="arw r"></div>
		<div class="arw l"></div>
		<a href="/media/photos" class="cls">
			close
		</a>
	</div>

</div>