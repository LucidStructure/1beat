<script type="text/javascript">
	function sI () {
		var wI = $(window).height();
		wI = wI *.9;
		$('.splash .stage').css('height',wI)
	}
	var sH = setInterval(sI, 10);

</script>
<div class="splash">
	<div class="stage">
		<div class="bg">
			<h2>ONEBEAT</h2>
			<h2 class="dt">2014</h2>
		</div>
	</div>
	<div class="statement">
		<h4>This October, the groundbreaking music diplomacy project OneBeat will bring 25 
		musicians from 17 countries around the world to the U.S. for a month of collaboration. 
		This year's OneBeat program begins with a two-week residency at the Montalvo Arts Center 
		in the Bay Area (Oct 8-22) followed by a two-week tour to Los Angeles, Arizona, and New Mexico. 
		In each of these locations the OneBeat musicians will spend several days giving performances, 
		leading youth workshops, and facilitating public music-making events.
		</h4>
	</div>
	<div class="dv"></div>
	<div class="fellows-14">
		<h2>2014 Fellows</h2>
		<a href="http://1beat.org/about/fellows/#2014" class="fellows"></a>
		<p>Hailing from seventeen countries, from Egypt to Indonesia to Venezuela, 
		this year's fellows include DJs and producers, traditional instrumentalists, 
		experimental musicians, indie-rockers, and more.  Check out the profiles of our <a class="chkout" href="http://1beat.org/1beast/about/fellows/#2014">
		2014 OneBeat fellows!</a>
		</p>
	</div>
	<div class="dv"></div>
	<div class="res-tour">
		<h2>Residency &amp; Tour</h2>
		<a href="http://1beat.org/about/program" class="res-tour"></a>
		<p>OneBeat is an exciting incubator for music-based ingenuity - 
		a think tank for some of the world's most promising artistic, technological, and social innovators. 
		The program begins with a two-week residency at the Montalvo Arts Center in Saratoga, CA, 
		where fellows will invent new music, record and produce tracks in custom-built mobile studios, 
		design participatory workshops, jam with local musicians, and brainstorm with Bay Area educators, 
		organizers, and entrepreneurs.  After the Residency, OneBeat will go on the road, touring from the 
		Bay Area south to Los Angeles, through Arizona to Albuquerque, and concluding with a final residency 
		in northern New Mexico.</p>
		<h3>October 8 - 21: Montalvo Arts Center, CA</h3>
		<h3>October 22 - 25: Los Angeles, CA</h3>
		<h3>October 27 - 28: Arcosanti, AZ</h3>
		<h3>October 29 - November 1: Albuquerque, NM </h3>
		<h3>November 2 - 4: Ghost Ranch, NM </h3>
		<p>At each tour location, OneBeat Fellows will perform the music they have developed during the residency, 
		lead workshops with local youth and community groups, and continue to record and rehearse their original music. 
		In addition, Fellows will get a chance to experience the unique musical culture of each location by working with 
		local musicians, attending concerts, and performing in public spaces ranging from outdoor music festivals to concert halls.</p>
	</div>
	<div class="dv"></div>
	<div class="events">
		<h2>Events and Performances</h2>
		<div class="event one">
			<div class="ou">
				<h2>Wednesday, October 15th</h2>
				<h3>San Francisco, CA</h3>
			</div>
			<div class="if">
				<h3>OneBeat SF</h3>
				<p>2 - 5pm  / Pop-up performances in iconic spaces around the city  /  Free</p>
				<p>7-11pm  /  Public Works  /  $10 suggested donation</p>
				<a href="https://www.facebook.com/profile.php?id=755772761153837">Invitation</a>
			</div>
			<div class="ds">
				<em>Found Sound Nation and the Global Lives Project present OneBeat SF, 
				a music-media event featuring 20+ artists and musicians mixing melodies, 
				vocals and beats against the backdrop of Global Lives video work. </em>
				<p>OneBeat musicians and staff partner with Bay Area independent filmmakers, 
				COMPOUND and the creative agency Projector to produce a series of spontaneous 
				live pop-up performances and video shoots throughout San Francisco.  
				Write <a href="mailto:1beat@foundsoundnation.org">1beat@foundsoundnation.org</a> for more details about locations.</p>
				<p>A one-of-a-kind audio-video jam, works performed at OneBeatSF are a radical re-imagining of 'world music' - 
				with works created exclusively for this event via a seven-day collaboration-convergence of artists and musicians 
				from around the globe. Colombian percussion, Arabic classical vocals, Ukrainian indie rock, 
				reimagined Kyrgyz folk tunes and more, will be juxtaposed against videos of everyday life 
				from around the world produced by the Global Lives Collective.</p>
			</div>
			<div class="lc">
				<p>Public Works</p>
				<p>161 Erie Street</p>
				<p>San Francisco, CA 94103</p>
				<a href="http://publicsf.com">publicsf.com</a>
			</div>
		</div>
		<div class="event two">
			<div class="ou">
				<h2>Sunday, October 19th</h2>
				<h3>Saratoga, CA</h3>
			</div>
			<div class="if">
				<h3>OneBeat Montalvo</h3>
				<p>11am - 4pm  /  Montalvo Arts Center  /  Free</p>
				<a href="https://www.facebook.com/events/836194506405573/">Invitation</a>
			</div>
			<div class="ds">
				<em>A multi-location music performance that explores the musical potential 
				of the extraordinary grounds of the Montalvo Arts Center.</em>
				<p>Part concert, part musical festival, the program will feature small group performances staged throughout Montalvo's 
				grounds and buildings that showcase unique new works created by OneBeat during their month-long 
				stay at the Lucas Artists Residency Program. Guests are invited to mingle among the musicians as 
				they perform and later gather in the Garden Theatre for a longer ensemble performance.</p>
			</div>
			<div class="lc">
				<p>Montalvo Arts Center</p>
				<p>15400 Montalvo Rd</p>
				<p>Saratoga, CA 95071</p>
				<a href="http://montalvoarts.org/">montalvoarts.org</a>
			</div>
		</div>
		<div class="event three">
			<div class="ou">
				<h2>Wednesday, October 22nd</h2>
				<h3>Los Angeles, CA</h3>
			</div>
			<div class="if">
				<h3>Sonos Showcase: Improvising Democracy</h3>
				<p>8 - 10pm  /  Sonos Studios  /  Free with RSVP</p>
			</div>
			<div class="ds">
				<em>Improvisation in the 21st Century - An Exploration of Improvisation and 
				Technology in partnership with Ableton and Blue Note.</em>
				<p>This interactive performance will feature a global mix of OneBeat musicians as they explore diverse styles of musical 
				improvisation from straight-ahead jazz to Cuban rumba to interaction with intelligent technologies, powered by 
				innovative music software Ableton Live. OneBeat uses musical improvisation as a means of international diplomacy, 
				as well as an allegory for freedom of expression, collective creativity, and authentic cross-cultural exchange. 
				Presented in partnership with Ableton, and celebrating the 75th Anniversary of Blue Note records.</p>
			</div>
			<div class="lc">
				<p>Sonos Studio</p>
				<p>145 N La Brea Ave</p>
				<p>Los Angeles, CA 90036</p>
				<a href="http://www.sonos-studio.com/">sonos-studio.com</a>
			</div>
		</div>
		<div class="event four">
			<div class="ou">
				<h2>Friday, October 24th</h2>
				<h3>Los Angeles, CA</h3>
			</div>
			<div class="if">
				<h3>Mariachi Plaza: Musical Muertos Mundiales</h3>
				<p>7-10 pm  /  Mariachi Plaza & Mbar  /   Free</p>
			</div>
			<div class="ds">
				<em>1Beat & the Boyle Heights Farmers Market present Musical Muertos Mundiales: 
				A celebration of musical cultures from around the world and from three historic 
				neighborhoods in downtown LA: Boyle Heights, the Arts District, and Little Tokyo.</em>
				<p>Colliding sounds and stories from around the world on the stage of one of LA's 
				most beloved public performance spaces. OneBeat at Mariachi Square features the music of 25 
				OneBeat Fellows -- young professional musicians from 17 countries who all share a desire 
				to use their music to make the world a better place -- in collaboration with young musicians 
				from the historic Boyle Heights & Little Tokyo neighborhoods. This concert will explore the 
				visible and invisible boundaries in the City of Angels and around the world, as well as ways 
				that music can be used to bridge cultural gaps with the power of groovy beats and mesmerizing melodies.  
				This will be followed by a Halloween dance after-party across the street at the M Bar. After party will 
				feature local Cumbia band A Poco No, plus DJ sets from 1Beat fellows & local tropical DJ, Fresko. </p>
			</div>
			<div class="lc">
				<div class="s">
					<p>Mariachi Plaza</p>
					<p>Pleasant Ave</p>
					<p>Los Angeles, CA 90033</p>
					<a href="http://mariachiplazalosangeles.com/site/">mariachiplazalosangeles.com</a>
				</div>
				<div class="s">
					<p>M Bar Boyle Heights</p>
					<p>1846 E 1st St</p>
					<p>Los Angeles, CA 90033</p>
					<a href="https://www.facebook.com/mbarboyleheights">Facebook</a>
				</div>
			</div>
		</div>
		<div class="event five">
			<div class="ou">
				<h2>Saturday, October 25th</h2>
				<h3>Los Angeles, CA</h3>
			</div>
			<div class="if">
				<h3>OneBeat Living Room Concert</h3>
				<p>8-11 pm  /  Art Share L.A.  /   Free</p>
			</div>
			<div class="ds">
				<em>A musical immersion in the global collaborative laboratory of OneBeat at Arts Share L.A.</em>
				<p>Welcome to the global collaborative laboratory of OneBeat, where 25 musicians from around the world will 
				fill the many rooms of the Art Share building with performances and sound installations that bridge sonic and social worlds.  
				Part house party, part participatory sound experiment, part geo-political dialogue, this unforgettable 
				evening will trace the musical connections between oft-estranged continents, neighborhoods, and ideologies.</p>
			</div>
			<div class="lc">
				<p>Art Share L.A.</p>
				<p>801 E 4th Pl</p>
				<p>Los Angeles, CA 90013</p>
			</div>
		</div>
		<div class="event six">
			<div class="ou">
				<h2>Tuesday, October 28th</h2>
				<h3>Arcosanti, AZ</h3>
			</div>
			<div class="if">
				<h3>OneBeat Arcosanti</h3>
				<p>7-9 pm (dinner at 6pm for $10)  /  Arcosanti  /   Free with RSVP</p>
			</div>
			<div class="ds">
				<em>This intimate performance will utilize the unique desert architecture and acoustics of Arcosanti, 
				as we explore themes of human beings' relationship with natural forces, 
				and reflect on how humanity has changed the ecology and landscape.</em>
				<p>Hailing from Egypt to Venezuela, this year's fellows include a percussionist and educator from Recife, 
				Brazil; a singer-songwriter and composer from Istanbul, Turkey; an experimental vocalist from Java, 
				Indonesia; and twenty-two other incredible musicians. We are excited to introduce you to this exceptionally diverse crew, 
				and to invite you to see and hear the explosion of new multimedia content that they will soon share with the world.  
				Admission is FREE, but registration is absolutely required.  Dinner will be available at 6 pm in the Arcosanti 
				Cafe @$10 per person and will include vegan/vegetarian/gluten-free options.</p>
			</div>
			<div class="lc">
				<p>Cafe At Arcosanti</p>
				<p>13555 S Cross L Rd</p>
				<p>Cordes Lakes, AZ 86333</p>
				<a href="https://arcosanti.org/">arcosanti.org</a>
			</div>
		</div>
		<div class="event seven">
			<div class="ou">
				<h2>Thursday, October 30th</h2>
				<h3>Albuquerque, NM</h3>
			</div>
			<div class="if">
				<h3>Sister Round Robin</h3>
				<p>8pm  /  Sister Bay Abq  /   Free</p>
			</div>
			<div class="ds">
				<p>Sister Bar will host musicians from across ABQ and around the world in a rolling, 
				spontaneous improv session, ranging from the funky to the sublime.</p>
				<p>A warm-up to our Nov 1 performance at the Rail Yards, this spontaneous round-robin improv session will bring together OneBeat musicians 
				-- 25 socially engaged musicians from 17 countries -- and a group of musicians from ABQ in an improv session that spans genres and continents. 
				Ranging from the funky to the sublime, the absurd to the awesome, come be a part of this unique music and geo-political dialogue, 
				and hang out with an incredible crew of musicians from every corner of the globe.</p>
			</div>
			<div class="lc">
				<p>Sister Bar</p>
				<p>407 Central Ave NW</p>
				<p>Albuquerque, NM 87102</p>
				<a href="http://sisterthebar.com/">sisterthebar.com</a>
			</div>
		</div>
		<div class="event eight">
			<div class="ou">
				<h2>Saturday, November 1st</h2>
				<h3>Albuquerque, NM</h3>
			</div>
			<div class="if">
				<h3>OneBeat Albuquerque</h3>
				<p>5-9pm  /  Rail Yards  /   $5 suggested donation</p>
			</div>
			<div class="ds">
				<p>Wedged in between Halloween and Dia de Los Muertos, this event will partner a global cadre musicians with a crew of 
				ABQ artists to celebrate a global diversity of musical and artistic expressions of the journeys of spirit.</p>
				<p>The theme of this year's OneBeat is the Audible Invisible, finding sonic expressions for the invisible powers that control us, 
				from gravity to electromagnetism to culture.  The Rail Yards show on November 1 - wedged between Halloween and Dia de Los Muertos - 
				will explore concepts of Spirit, one of the great invisible human universals. 
				While the OneBeat musicians come from radically different backgrounds and religious beliefs (or lack thereof), 
				they are all fascinated with the journey of spirit and particularly the notions of "thresholds" which spirits cross in life, death and other transformations. 
				Interwoven with this musical performance will be art and video installations along these same themes. 
				Featured visual artists include Augustine Romero, Michelle Montjoy, Ellen Babcock and others to be announced. </p>
				<p>Save the date and don't miss OneBeat Albuquerque!</p>
			</div>
			<div class="lc">
				<p>Albuquerque Rail Yards</p>
				<p>Blacksmith Shop ("The Yards")</p>
				<p>777 1st St. SW (between Stover & Hazeldine Streets south of Coal Ave.)</p>
				<a href="https://www.facebook.com/TheAlbuquerqueRailyards">Facebook</a>
			</div>
		</div>
	</div>
	<div class="dv"></div>
	<div class="social">
		<h2>Follow Us</h2>

			<ul>
				<li class="l"><a href="https://www.facebook.com/1beatmusic" class="fb"></a><span><a href="https://www.facebook.com/1beatmusic">Join On Facebook</a></span></li>
				<li><a href="https://twitter.com/1beatmusic" class="twt"></a><span><a href="https://twitter.com/1beatmusic">Follow On Twitter</a></span></li>
			</ul>
			<ul class="sub">
				<li class="bf"><a href="http://facebook.com/1beatmusic">http://facebook.com/1beatmusic</a></li>
				<li class="wtw"><a href="http://twitter.com/1beatmusic">http://twitter.com/1beatmusic</a></li>
			</ul>
			<h2>#1beat</h2>
		</div>
	<div class="dv md"></div>
	<h2>2014 Partners &amp; Supporters</h2>
	<div class="sponsors">
		<img src="ui/i/sponsors-new.jpg" />
		<!-- <div class="sml cf">
			<div class="one cf">
				<a href="http://montalvoarts.org/" class="logo-p mont"></a>
				<a href="http://516arts.org/" class="logo-p fos"></a>
				<a href="https://arcosanti.org/" class="logo-p arco"></a>
				<a href="http://artsharela.org/" class="logo-p art-shr"></a>
			</div>
			<div class="two cf">
				<a href="http://ghostranch.org/" class="logo-p ghost-ranch"></a>
				<a href="https://www.nhccfoundation.org/" class="logo-p nhccf"></a>
				<a href="" class="logo-p yards"></a>
				<a href="https://www.ableton.com/" class="logo-p abl"></a>
			</div>
			<div class="three cf">
				<a href="http://www.pelusomicrophonelab.com/" class="logo-p pel"></a>
				<a href="http://worldup.org/" class="logo-p world-up"></a>
				<a href="http://to.be/" class="logo-p tobe"></a>
				<a href="http://www.glyphtech.com/" class="logo-p glyph"></a>
				<a href="http://www.daddario.com/DaddarioSplash.Page?ActiveID=1740" class="logo-p dard"></a>
				<a href="http://www.dubspot.com/" class="logo-p dspot"></a>
			</div>
		</div> -->
	</div>
</div>