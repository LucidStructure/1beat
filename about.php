
	<div class="w_slide">
		<div class="slide"></div>
		<div class="slide"></div>
		<div class="slide">
			<!-- <video src="https://s3.amazonaws.com/onebeatslideshow/vid/equip3-test1.webm" poster loop autoplay="autoplay"></video> -->
		</div>
		<div class="slide"></div>
		<div class="excerpt">
			<div class="text">
				<h3>OneBeat brings musicians from around the world to the US to collaborate, perform and exchange ideas</h3>
			</div>
			<div class="text">
				<h3>OneBeat consists of a two week Residency, followed by a two-week tour.</h3>
			</div>
			<div class="text">
				<h3>OneBeat invites musicians from a diversity of musical styles: rock, folk, experimental hip hop, electronic, jazz, classical, and more</h3>
			</div>
			<div class="text">
				<h3>Sign up for the OneBeat newsletter to get more info and updates.</h3>
			</div>
		</div>
	</div>
	
	<div id="scroll-down">
		<span>scroll</span>
	</div>
