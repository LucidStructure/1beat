<?php 

$o = isset($_GET['o']) ? $_GET['o'] : FALSE;

?>
<div class="nav about">
	<ul>
		<li class="first"><a href="/about/mission" <?php if($o == 'mission') { echo "class='active'"; }?>>Mission</a></li>
		<li><a href="/about/program" <?php if($o == 'program') { echo "class='active'"; }?>>Program</a></li>
		<li><a href="/about/fellows/#2014" <?php if($o == 'fellows') { echo "class='active'"; }?>>Fellows</a></li>
		<li><a href="/about/staff/#2014" <?php if($o == 'staff') { echo "class='active'"; }?>>Team</a></li>
		<li><a href="/about/archives/#2014" <?php if($o == 'archives') { echo "class='active'"; }?>>Archives</a></li>
		<li><a href="/about/quotes" <?php if($o == 'quotes') { echo "class='active'"; }?>>Quotes</a></li>
		<li><a href="/about/resources#resources" <?php if($o == 'resources') { echo "class='active'"; }?>>Resources</a></li>
		<li><a href="/about/partners/#partners" <?php if($o == 'partners') { echo "class='active'"; }?>>Partners</a></li>
		<li><a href="/about/apply#apply" <?php if($o == 'apply') { echo "class='active'"; }?>>Apply</a></li>
		<li class="last"><a href="/about/contact" <?php if($o == 'contact') { echo "class='active'"; }?>>Contact</a></li>
	</ul>
</div>