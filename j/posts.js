var output = ''

var num = 1

$(document).ready(function() {

	$(".content.blog").append('<div id="more-blog" class="more blog" ><span>more +</span></div>')
	$('.more.blog').click(function() {
		console.log("shi")
		$.getJSON('../ui/j/json/people.json', function(data) {
			$.getJSON('../ui/j/json/blog.json', function(blog) {
				var count = 0
				var len = blog.post.length
				var text = ''
				for (var i = num * 10; i < len && count < 10; i++) {
					text = printPost(blog, data, i)
					count++
				}
				num++
				if(i >= blog.feat.length) {
					console.log("removal")
					$("#more-blog").remove()
				}


				document.getElementById("blog-wrp").innerHTML = text; //creative magic

			})
		})
	});


	function printPost(blog, data, id) {

		output += '<div class="blog post">'
		output += '<div class="info">'
		if (blog.post[id].video != undefined) {
			output += '<div class="video">'
			output += blog.post[id].video
			output += '</div>'
		} else {
			output += '<a class="image" href="' + blog.post[id].url + '"><img src="' + blog.post[id].pic + '">'
		}
		if(blog.post[id].credit != undefined) {
			output += '<div class="credit">credit:' + blog.post[id].credit + '</div>'
		}


		output += '<a class="title" href="http://1beat.org/community/posts#' + id + '" target="_blank">' + blog.post[id].headline + '</a>'
		output += '<h4 class="date">' + disDate(blog.post[id].date) + '</h4>'
		
		output += '<div class="social">'
		var names = blog.post[id].name.split(",")
		for (var n in names) {
			for (var j in data.people) {
				if (names[n].replace(/ /g,'') == data.people[j].name.replace(/ /g,'')) {
					output += '<ul>'
					
					var perurl = document.URL.substring(0,document.URL.indexOf("community")) + "about/people/p/#" + names[n].replace(/ /g, '')
					
					output += '<li><a href="'+ perurl + '">' + names[n] + '</a> | OneBeat ' 
					if (data.people[j].year != undefined) {
						output += data.people[j].year + ' '
					}
					output += data.people[j].type + '</li>'
					

					// if (data.people[j].website != undefined) {
					// 	output += '<li><a href="' + data.people[j].website + '" target="_blank">website</a> |</li>'
					// }

					// if (data.people[j].twitter != undefined) {
					// 	output += '<li><a href="' + data.people[j].twitter + '" target="_blank">twitter</a> |</li>'
					// }
					// if (data.people[j].soundcloud != undefined) {
					// 	output += '<li><a href="' + data.people[j].soundcloud + '" target="_blank">soundcloud</a> |</li>'
					// }
				break//must test
				}
			}
		}



		output += '</div><p>' + blog.post[id].description + '</p>'

		
		output += '<div class="social">'
		for (var n in names) {
			for (var j in data.people) {
				if (names[n].replace(/ /g,'') == data.people[j].name.replace(/ /g,'')) {
					output += '<ul>'
					

					if (data.people[j].website != undefined) {
						output += '<li><a href="' + data.people[j].website + '" target="_blank">website</a> |</li>'
					}

					if (data.people[j].twitter != undefined) {
						output += '<li><a href="' + data.people[j].twitter + '" target="_blank">twitter</a> |</li>'
					}
					if (data.people[j].soundcloud != undefined) {
						output += '<li><a href="' + data.people[j].soundcloud + '" target="_blank">soundcloud</a> |</li>'
					}
				break
				}
			}
		}
		output += '</div>'
		output += '</div>'
		output += '</div>'
		output += '<div class="dv"></div>'
		return output
	}

	var months =  ["January" ,"February" ,"March" ,"April" ,"May" ,"June" ,"July" ,"August" ,"September" ,"October" ,"November" ,"December"]

	function rmz(num) {
		if(num[0] == "0") {
			//console.log(num[1])
			return num[1]
		} else {
			return num
		}
	}
	function disMonth(month) {
		//console.log(months[rmz(month)])
		return months[rmz(month-1)]
	}


	function disDate(date) {
		var d = ''
		if(date != undefined) {
			var nums = date.split(".")
			//console.log(nums)
			d += disMonth(nums[1])
			d += ' '
			d += rmz(nums[0])
			d += ', '
			d += nums[2]
		}
		return d
	}

	$.getJSON('../ui/j/json/people.json', function(data) {
		$.getJSON('../ui/j/json/blog.json', function(blog) {


			var text = ''
			var id = document.URL.substring(document.URL.lastIndexOf("#") + 1);
			console.log(id)
			if (id != document.URL) {
				text += printPost(blog, data, id)
			}
			document.getElementById("blog-wrp").innerHTML = text; //creative magic
			
		});
	});


});