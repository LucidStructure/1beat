
var output = ''

var num = 1

$(document).ready(function() {
	console.log("pre click")
	$('.more.blog').click(function() {
		console.log("click test")
		$.getJSON('../ui/j/json/people.json', function(data) {
			$.getJSON('../ui/j/json/blog.json', function(blog) {
				var count = 0
				var len = blog.feat.length
				var text = ''
				for (var i = num * 10; i < len && count < 10; i++) {
					text += printPost(blog, data, i)
					count++
				}
				num++
				
				document.getElementById("blog-wrp").innerHTML += text; //creative magic

			})
		})
	});


	function printPost(blog, data, id) {

		

		var output = ''
		if (id == 0) {
			output += '<div class="blog post first">'
		} else {
			output += '<div class="blog post">'
		}
		if(location.hash != "") {
			id = blog.feat.length - id
		}
		output += '<div class="info">'
		if (blog.feat[id].video != undefined) {
			output += '<div class="video">'
			output += blog.feat[id].video
			output += '</div>'
		} else {
			output += '<a class="image" href="' + blog.feat[id].url + '"><img src="' + blog.feat[id].pic + '">'
		}
		if(blog.feat[id].credit != undefined) {
			output += '<div class="credit">credit:' + blog.feat[id].credit + '</div>'
		}


		output += '<a class="title" href="http://1beat.org/community/blog#' + (blog.feat.length - id) + '" target="_blank">' + blog.feat[id].headline + '</a>'
		output += '<h4 class="date">' + disDate(blog.feat[id].date) + '</h4>'

		output += '<div class="social">'
		var names = blog.feat[id].name.split(",")
		if(blog.feat[id].headline == "Mixtape 2013") {} else { 
			for (var n in names) {
				for (var j in data.people) {
					if (names[n].replace(/ /g,'') == data.people[j].name.replace(/ /g,'')) {
						output += '<ul>'
						
						var perurl = document.URL.substring(0,document.URL.indexOf("community")) + "about/people/p/#" + names[n].replace(/ /g, '')
						
						output += '<li><a href="'+ perurl + '">' + names[n] + '</a> | OneBeat ' 
						if (data.people[j].year != undefined) {
							output += data.people[j].year + ' '
						}
						output += data.people[j].type + '</li>'
						

						// if (data.people[j].website != undefined) {
						// 	output += '<li><a href="' + data.people[j].website + '" target="_blank">website</a> |</li>'
						// }

						// if (data.people[j].twitter != undefined) {
						// 	output += '<li><a href="' + data.people[j].twitter + '" target="_blank">twitter</a> |</li>'
						// }
						// if (data.people[j].soundcloud != undefined) {
						// 	output += '<li><a href="' + data.people[j].soundcloud + '" target="_blank">soundcloud</a> |</li>'
						// }
					//must test
					}
				}
			}
		}



		output += '</div><p>' + blog.feat[id].description + '</p>'

		
		output += '<div class="social">'
		if(names.length <= 1) { 
			for (var n in names) {
				for (var j in data.people) {
					if (names[n].replace(/ /g,'') == data.people[j].name.replace(/ /g,'')) {
						output += '<ul>'
						

						if (data.people[j].website != undefined) {
							output += '<li><a href="' + data.people[j].website + '" target="_blank">website</a> |</li>'
						}

						if (data.people[j].twitter != undefined) {
							output += '<li><a href="' + data.people[j].twitter + '" target="_blank">twitter</a> |</li>'
						}
						if (data.people[j].soundcloud != undefined) {
							output += '<li><a href="' + data.people[j].soundcloud + '" target="_blank">soundcloud</a> |</li>'
						}
					
					}
				}
			}
		}
		output += '</div>'
		output += '</div>'
		output += '</div>'
		output += '<div class="dv"></div>'

		//console.log(output)
		return output
	}

	var months =  ["January" ,"February" ,"March" ,"April" ,"May" ,"June" ,"July" ,"August" ,"September" ,"October" ,"November" ,"December"]

	function rmz(num) {
		if(num[0] == "0") {
			//console.log(num[1])
			return num[1]
		} else {
			return num
		}
	}
	function disMonth(month) {
		//console.log(months[rmz(month)])
		return months[rmz(month-1)]
	}


	function disDate(date) {
		var d = ''
		if(date != undefined) {
			var nums = date.split(".")
			//console.log(nums)
			d += disMonth(nums[1])
			d += ' '
			d += rmz(nums[0])
			d += ', '
			d += nums[2]
		}
		return d
	}

	var count = 0; 
	var d;
	var b; 
	$.getJSON('../ui/j/json/people.json', function(data) {
		$.getJSON('../ui/j/json/blog.json', function(blog) {
			d = data;
			b = blog;

			var text = ''
			var id = document.URL.substring(document.URL.lastIndexOf("#") + 1);
			if (id != document.URL) {
				text += printPost(blog, data, id)
			} else {
				for (var i in blog.feat) {
					if (i < 10) {
						text += printPost(blog, data, i)
					} 
				}
				if(blog.feat.length <= 10) {
					$(".content.blog").remove()
				}
				
			}
			document.getElementById("blog-wrp").innerHTML = text; //creative magic

		});
	});


});