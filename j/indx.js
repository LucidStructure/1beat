	var num = 0

	var ppl

	var repeat = true
	var index = '<tr> \
				<th class="name"> \
					Name \
					<div class="filter" name="name"> \
						<div class="arw active" name="name"></div> \
						<div class="cls" name="name"></div> \
						<div class="cls arw" name="name"></div> \
						<div class="opn" name="name"></div> \
						<ul class="name"> \
							<li class="one">2012 Fellows</li> \
							<li class="two">2013 Fellows</li> \
							<li class="three">Collaborators</li> \
							<li class="four">Staff</li> \
							<li class="five">Int. Partner</li> \
							<li class="six">Web Featured</li> \
						</ul> \
					</div> \
				</th> \
				<th class="city"> \
					City \
				</th> \
				<th class="country"> \
					Country \
					<div class="filter" name="country"> \
						<div class="arw active" name="country"></div> \
						<div class="cls" name="country"></div> \
						<div class="cls arw" name="country"></div> \
						<div class="opn" name="country"></div> \
						<div class="ctr-wrp"> \
						<ul class="country">'

	$(document).ready(function() {


		function printRows(person, i) {

			if (person != undefined) {

				var url = document.URL.substring(0, document.URL.indexOf("index"));
				var blogurl = url + "blog#" + i
					//console.log(perurl)


				index += '<tr>'
				if (person.type == "Fellow") {
					index += '</td><td class="name active">'
				} else {
					index += '</td><td class="name">'
				}
				perurl = document.URL.substring(0, document.URL.indexOf("community")) + "community/people/ppl/#" + person.name.replace(/ /g, '')

				if (person.type == "Fellow") {
					index += '<a class="active" href="' + perurl + '">' + person.name + '</a> </td>  <td class="inst">'
				} else {
					index += '<a href="' + perurl + '">' + person.name + '</a> </td>  <td class="inst">'
				}

				index += '</td><td class="city">'
				if (person.city != undefined) {
					index += person.city
				}

				index += '</td><td class="country">'
				if (person.country != undefined) {
					index += person.country
				}
				index += '</td><td class="region">'

				if (person.region != undefined) {
					index += person.region
				}

				index += '</td><td class="instrument">'
				if (person.instrument != undefined) {
					index += person.instrument
				}

				index += '</tr>' //end row

				return index

			}

		}



		function name(ppl, num, val) {

			var count = 0
			for (var i = num; i < ppl.length && count < 30; i++) {
				var match = 0
				if (ppl[i].type != undefined) {
					if (val.indexOf("Collab") > -1) {
						match = ppl[i].type == "Collaborating Artist"
					} else if (val == "Staff") {
						match = ppl[i].type == "Staff"
					} else if (val == "Int.Partner") {
						match = ppl[i].type == "International Partner"
					} else if (val == "WebFeatured") {
						match = ppl[i].type == "Web-featured artist"
					}
					else {
						match = val.indexOf(ppl[i].type) > -1 && val.indexOf(ppl[i].year) > -1
					}
				}

				if (match) {
					count++
					text = printRows(ppl[i], i)
				}
			}

			if (text != undefined) {
				document.getElementById("indx").innerHTML = text;
			}
			return [count, num]
		}



		function country(ppl, num, val) {

			var count = 0
			for (var i = num; i < ppl.length && count < 30; i++) {
				
						var match = 0
						if (ppl[i].country != undefined) {
							match = ppl[i].country.replace(/ /g, '').indexOf(val) > -1
						}
						if (match) {
							text = printRows(ppl[i], i)
							count++
							if (count == 30) {
								num = i
							}
						}
			}
			if (text != undefined) {
				document.getElementById("indx").innerHTML = text;

			}
			return [count, num]
		}



		function region(ppl, num, val) {
			var count = 0
			for (var i = num; i < ppl.length && count < 30; i++) {
				var match = 0
				var regions = ["Africa", "WesternHemisphere", "NearEastAsia", "EastAsiaPacific", "Europe"]
				var reg = ["AF", "WHA", "NEA", "EAP", "EUR"]
				var real = reg[regions.indexOf(val)]
				if (ppl[i].region != undefined) {
					match = ppl[i].region == real
				}
				if (match) {
					text = printRows(ppl[i], i)
					count++
					if (count == 30) {
						num = i
					}
				}
			}
			if (text != undefined) {
				document.getElementById("indx").innerHTML = text;
			}
			return [count, num]

		}



		function date(ppl, num, val) {
			var count = 0
			if (val == "newest") {
				for (var i = num; i < ppl.length && count < 30; i++) {
					text = printRows(ppl[i], i)
					count++
					if (count == 30) {
						num = i
					}
				}
			}
			if (text != undefined) {
				document.getElementById("indx").innerHTML = text;
			}
			//console.log(i)
			return [count, num]
		}

		$.getJSON('../ui/j/json/people.json', function(data) {

			$.getJSON('../ui/j/json/blog.json', function(blog) {

				ppl = shuffle(data.people)

				for (var a in data.countries) {
					index += '<li>' + data.countries[a] + '</li>'
				}

				index += '</ul> \
					</div> \
					</div> \
				</th> \
				<th class="region"> \
					Region \
					<div class="filter" name="region"> \
						<div class="arw active" name="region"></div> \
						<div class="cls arw" name="region"></div> \
						<div class="cls" name="region"></div> \
						<div class="opn" name="region"></div> \
						<ul class="region"> \
							<li class="one">Africa</li> \
							<li class="two">Western Hemisphere</li> \
							<li class="three">Near East Asia</li> \
							<li class="four">East Asia Pacific</li> \
							<li class="four">Europe</li> \
						</ul> \
					</div> \
				</th> \
				<th class="instrument"> \
					Discipline \
				</th> \
			</tr>';

				var url = document.URL.substring(document.URL.lastIndexOf("#") + 1);
				var filter = url.split("-")

				var cat = filter[0]
				var val = filter[1]


				if (cat == "name") {
					count = name(ppl, 0, val)
				} else if (cat == "country") {
					count = country(ppl, 0, val)
				} else if (cat == "region") {
					count = region(ppl, 0, val)
				} else if (cat == "date") {
					if (val == "oldest") {
						count = date(ppl, ppl.length + 1, val)
					} else {
						count = date(ppl, 0, val)
					}

				}

				document.getElementById("indx").innerHTML = index;

			});
		});


		var url = ''

		function clicks() {

			//filter
			$('table.index th .filter .opn').click(function() {
				var typ = $(this).attr('name');
				$(this).addClass("off")
				$('table.index th .filter ul.' + typ + ' li').addClass('active');
				$('table.index th.' + typ + ' .filter .cls').addClass('active');
			});

			$('table.index th .filter .cls').click(function() {
				var typ = $(this).attr('name');
				$('table.index th .filter ul.' + typ + ' li').removeClass('active');
				$(this).removeClass('active');
				$('table.index th.' + typ + ' .filter .opn').removeClass('off');
				$('table.index th.' + typ + ' .filter .cls').removeClass('active');

			});

			//arrws
			$('table.index th .filter .arw').click(function() {
				var typ = $(this).attr('name');
				$(this).removeClass('active');

				$('table.index th .filter ul.' + typ + ' li').addClass('active');
				$('table.index th.' + typ + ' .filter .cls').addClass('active');
				$('table.index th.' + typ + ' .filter arw.cls').addClass('active');

				$('table.index th .filter .opn[name=' + typ + ']').addClass("off");

				console.log(typ)

			});
			$('table.index th .filter .arw.cls').click(function() {
				var typ = $(this).attr('name');
				$(this).removeClass('active');


				$('table.index th .filter .arw').addClass('active');
				$('table.index th .filter ul.' + typ + ' li').removeClass('active');
				$('table.index th.' + typ + ' .filter .opn').removeClass('off');
				$('table.index th.' + typ + ' .filter .cls').removeClass('active');

				console.log(typ)

			});
		}
		setInterval(clicks, 100);

		function filter() {
			$('table.index th .filter ul li').click(function() {
				var par = $(this).parent().attr('class');
				par = par + '-'
				var typ = $(this).html();
				typ = typ.replace(/ /g, '');
				var hash = par + typ;
				window.location.hash = hash;
				window.location.reload();

				num = 0
				repeat = true

			});
		}
		setInterval(filter, 100);


		$('.more.indx').click(function() {
			var url = document.URL.substring(document.URL.lastIndexOf("#") + 1);
			var filter = url.split("-")

			var cat = filter[0]
			var val = filter[1]

			if (count[0] == 30 || count[0] == undefined) {
				num = count[1] + 1
				if (cat == "name") {
					count = name(ppl, num, val)
				} else if (cat == "country") {
					count = country(ppl, num, val)
				} else if (cat == "region") {
					count = region(ppl, num, val)
				} else if (cat == "date") {
					count = date(ppl, num, val)
				}
			}

		});

		//setInterval(more, 100);



		// var filter = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
		// var output 
		// if(filter == 'recent') {

		// }
	});


	function shuffle(o) { //v1.0
		for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
		return o;
	};


	/*
function getDrop(cat, val) {
	$.getJSON('../ui/j/json/people.json', function(data) {
		//consider unsing a switch
		if(cat == "name") {//show types
			for(var i in data.people) {
				for(var i in blog.post) {
					for(var k in blog.post[i].artist) {
						var name = blog.post[i].artist[k].split(" ");
				
						if(data.people[i].type == val) {}
					}
				}
			}
		} else if (cat == "date") {
			for(var i in data.people) {
				if(data.people[i]. == val) {}
			}
		}
	});
}*/