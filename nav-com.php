<?php
	$o = isset($_GET['o']) ? $_GET['o'] : FALSE;
?>
<div class="filter about">
    <ul>
      <li><a href="/community/blog" <?php if($o == 'blog') { echo "class='active'"; }?>>Blog</a></li>
      <li><a href="/about/fellows/#2014" <?php if($o == 'fellows') { echo "class='active'"; }?>>Fellows</a></li>
      <li><a href="/community/echo" <?php if($o == 'echo') { echo "class='active'"; }?>>Echo</a></li>
      
    </ul>
    <?php 
    	if($o == 'blog' || $o == 'posts') {
    		echo "<p>Our Blog features the ongoing work of OneBeat Fellows and other musicians in the OneBeat community.</p>";
    	} else if($o == 'map') {
    		echo "<p>Our interactive map displays OneBeat Fellows, partners, and organizations. Check out our OneBeat <a href=\"/community/index#date-newest\">Index</a></p>";
    	} else if($o == 'index') {
    		echo "<p>Our Index provides a simple way to find information about the entire OneBeat community.</p>";
    	}
      ?>
</div>



