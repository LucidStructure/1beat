<div class="echo">
<div class="sss_f">
	<div class="sli">
		<div class="s f stl one" name="one">
				<div class="cvr" <img width="100%" src="https://s3-us-west-2.amazonaws.com/1beast/2015/Echo-banner-.png"/></div>
			<div class="wrd">
				<img src="https://s3-us-west-2.amazonaws.com/1beast/OneBeat_LOGO_white.png" />
				<h1>OneBeat</h1>
				<h2>((ECHO))</h2>
				<a href="http://1beat.org/echo" class="watch">Check it Out!</a>
			</div>
		</div>
	</div>
</div>
</div>

<div class="echo-blurb">
	<h4>Echo is an online exchange modeled after OneBeat.  Echo connects innovative musicians, artists, writers, and thinkers in a month-long collaboration, during which time Echo Fellows engage in an online laboratory of artistic and scholarly ideas and experimentation. </h4>
	<a href="https://docs.google.com/forms/d/1kKfxeZoXoWWvV4o86JZp2nGORsxe-fB4ZRQIdCa2huA/viewform?usp=send_form" class="watch">Call for participation!</a>
</div>
<div class="dv2"></div>
<div class="content echo-info">
	<h1> Echo 2014 Collaborations </h1>
	<iframe width="50%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/76185847%3Fsecret_token%3Ds-zovTt&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
	<div class="dv2"></div>
	<h1>Echo 2014 Fellows </h1>
	<div class="slide fellows cf three a" name="fellows">
		<div class="info one">
			<!-- <h3>Echo Fellows</h3> -->
			<div class="ensemble one">
				<p><strong>Ensemble 1</strong></p>
				<p>Natasha Humera Ejaz -  Producer, Vocalist from Pakistan</p>
				<p>Roshy Kheshti - Academic from USA</p>
				<p>Svetlana Maras - Composer from Serbia</p>
				<p>Steven Baker - Academic from USA </p>
			</div>
			<div class="ensemble two">
				<p><strong>Ensemble 2</strong></p>
				<p>PPS - MC from Senegal</p>
				<p>Sasha Grbich - Academic from Australia</p>
				<p>Fuerza Mexica - Emcee from USA</p>
				<p>Dmitri Burmistrov - Beatboxer, Producer from Russia</p>
			</div>
			<div class="ensemble three">
				<p><strong>Ensemble 3</strong></p>
				<p>Malabika Brahma  - Vocalist from India</p>								
				<p>Giorgos Stylianou - Lute player, Sound Artist from Cyprus </p>
				<p>Shannon Werle  -  Academic from USA </p>
			</div>
			<div class="ensemble four">
				<p><strong>Ensemble 4</strong></p>
				<p>Lama Zakharia &amp; Jana Zeinedinne  -  Vocalists from Jordan</p>
				<p>Helio Vanimal - Singer, Instrumentalist from Mozambique</p>
				<p>Jay Hammond - Banjo Player, Academic from USA</p>
				<p>Helen Morgan - Artist, Sounder from United Kingdom</p>
			</div>
			<div class="ensemble five">
				<p><strong>Ensemble 5</strong></p>
				<p>Wei Wei - Composer from China </p>
				<p>Elen Flügge - Writer, Sound Artist from Germany/USA </p>
				<p>Weronika Partyka -  Singer, Clarinet Player from Poland </p>
				<p>Aditi Bhagwat -  Percussionist from India </p>
			</div>
			<div class="ensemble six">
				<p><strong>Ensemble 6</strong></p>
				<p>Aimilia Karapostoli - Academic from Greece </p>
				<p>Ruth Danon - Vocalist, Producer from Israel </p>
				<p>Leah Barclay - Composer, Sound Artist from Australia </p>
				<p>Sarah Alden - Violinist, Singer from USA</p>
			</div>
			<div class="ensemble seven">
				<p><strong>Ensemble 7</strong></p>
				<p>Nicole Hodges Persley - Academic, Theater Artist from USA</p>
				<p>Ahmed Mabrok - MC from Egypt </p>
				<p>Ruben Coll -  Researcher, Sound Activist from Spain</p>
				<p>Wey Chong Chua - Ruan from Malaysia </p>
			</div>
			<div class="ensemble eight">
				<p><strong>Ensemble 8</strong></p>
				<p>Elyse Tabet - Producer, Sound Artist from Lebanon</p>
				<p>Grupal Crew Collective - Artists from Columbia, Italy, and Spain</p>
				<p>Sharrell Luckett - Academic, Performance Artist from USA </p>
				<p>Amine Metani - Electronic Music Producer from Tunisia</p>
			</div>
			<div class="ensemble nine">
				<p><strong>Ensemble 9</strong></p>
				<p>Darbuka Siva -  Drummer, Bassist from India</p>
				<p>Laura Plana Gracia - Academic from United Kingdom </p>
				<p>Jeb Middlebrook - Sound Archivist, Researcher from USA</p>
			</div>				
		</div>	
	</div>
	<div class="dv2"></div>
	<div class="echo-about">
		<h1>OneBeat Echo 2014</h1>
		<p>OneBeat Echo 2014 was co-created and curated by Jeb Middlebrook, Assistant Professor of Sociology at California State University, Dominguez Hills in Los Angeles, where he researches and teaches music, justice, and ethnic studies. He holds a Ph.D. and M.A. in American Studies and Ethnicity from the University of Southern California, and a B.A. in Ethnic Studies from the University of Minnesota. His current work explores the sound of prison in popular culture, policy, and protest.  The 2014 process took the following structure. </p>
		<p>I. Sharing: In the first stage of Echo, Fellows were asked to offer existing creative or scholarly work that they would like to share with the Echo community, reflecting the mission of OneBeat: "to celebrate the transformative power of the arts through the creation of original, inventive music; to develop ways that music can make a positive impact on our local and global communities; and to foster mutual understanding and cooperation across geographic and cultural barriers."</p>
		<p>II. Collaborating: In the second stage of Echo, Fellows were randomly assigned to "Ensembles"  (small groups of Fellows) and asked to get to know each other via email, Skype, text and existing social media platforms, and then to collaborate by responding to one another's work.  This year, the collaboration was based off an existing platform developed by web designer Cameron Steele and OneBeat director Chris Marianetti called Telephenesis.  Telephenesis is an online space for musical collaboration modeled after the children's game of telephone.  Instead of words, music gets mimicked and reinterpreted.  Collaborations exist as connected systems of stars, forming constellations.  </p>
		<a href="http://1beat.org/echo/"><img src="https://s3-us-west-2.amazonaws.com/1beast/echo-screenshot.jpg" /></a>
		<p>III. Exhibiting: In the third and last stage of Echo, the collective creative works of all Echo Fellows are updated on the Echo website for the global public to view, share, and experience.  In future iterations we hope to also create a platform where the wider public can further engage and collaborate creatively with Fellows.  </p>
		
	</div>
</div>


