<script type='text/javascript' src='http://d3js.org/d3.v3.min.js'></script>
  	<script type='text/javascript' src='http://d3js.org/topojson.v1.min.js'></script>
  	<script type='text/javascript' src='/ui/j/planetaryjs.js'></script>
	
<div class="network">
	<div class="place-wrap">
		<div id="placeholder">
			<div class="more"></div>
		</div>
	</div>
</div>

<div class="person info" id="info">
	
</div>
  
<canvas id='rotatingGlobe'></canvas>
<p class="drag">drag cities into the center of the globe</p>
<div class="view">
	<div class="crc"></div>
</div>
