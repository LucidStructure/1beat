<script>
$(document).ready(function() {

	function printPost(blog, data, id) {

		var output = ''
		if (id == 0) {
			output += '<div class="blog post first">'
		} else {
			output += '<div class="blog post">'
		}
		output += '<div class="info">'
		if (blog.post[id].video != undefined) {
			output += '<div class="video">'
			output += blog.post[id].video
			output += '</div>'
		} else {
			output += '<a class="image" href="' + blog.post[id].url + '">' 
			if(blog.post[id].pic != undefined) {
				output += '<img src="' + blog.post[id].pic + '">'
			}
		}
		if(blog.post[id].credit != undefined) {
			output += '<div class="credit">credit:' + blog.post[id].credit + '</div>'
		}


		output += '<a class="title" href="http://1beat.org/community/posts#' + id + '" target="_blank">' + blog.post[id].headline + '</a>'
		output += '<h4 class="date">' + blog.post[id].date + '</h4>'
		output += '<p>' + blog.post[id].description + '</p>'
		output += '<div class="social">'
		
		output += '</div>'
		output += '</div>'
		output += '</div>'
		output += '<div class="dv2"></div>'
		return output
	}


	var user = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
	var str = user
	var output = ''
		$.getJSON('../../../ui/j/json/people.json', function(data) {
			$.getJSON('../../../ui/j/json/blog.json', function(blog) {
			for(var j in data.people) {
				
				if(data.people[j].name.replace(/ /g,'').toLowerCase() == str.replace(/ /g,'').toLowerCase()) {
					output += '<div class="content extend person">'
					output += '<div class="person">'
				
					output += '<h2>'+data.people[j].name+'</h2><h3>OneBeat ' 
					if(data.people[j].year != undefined) {
						output += data.people[j].year + ' '
					}
					if(data.people[j].type != undefined) {
						output += data.people[j].type 
					}
					output += '</h3><h3>'
					if(data.people[j].instrument != undefined) {
						output +=  data.people[j].instrument 
					} 
					if(data.people[j].city != undefined) {
						output += ' | ' + data.people[j].city 
						//NESTED FOR THE BAR ||||||
						if(data.people[j].country != undefined) {
							output += ', ' + data.people[j].country 
						}
					} else if(data.people[j].country != undefined) {
							output += ' | ' + data.people[j].country 
					}
					output += '</h3>'


					output += '<div class="social"><ul>'
					if(data.people[j].website != undefined) {
						output += '<li><a href="'+data.people[j].website+'" target="_blank">website</a> | </li>'
		 			}

					if(data.people[j].twitter != undefined) {
						output += '<li><a href="'+data.people[j].twitter+'" target="_blank">twitter</a> | </li>'
		 			}
					if(data.people[j].soundcloud != undefined) {
						output += '<li><a href="'+data.people[j].soundcloud+'" target="_blank">soundcloud</a> | </li>'
		 			}
					output += '</ul></div>';

					if(data.people[j].bio != undefined) {
						output += '<div class="bio">'+data.people[j].bio + '</div>'
					}

					output += '</div></div></div>';
					


					output +='<h2 class="fp">Featured Posts</h2> \
								<div class="dv"></div>';

					document.getElementById("person").innerHTML+=output;
								

					
					break;
				}
			}
			var text = ''
			for (var i = 0; i < blog.post.length-1; i++) {
				//console.log(data.people[j].name)
				if(blog.post[i].name == data.people[j].name) {
					text += printPost(blog, data, i)
				}
			}
			text = text.substring(0, text.length-23);
			
			document.getElementById("blog-wrp").innerHTML = text; //creative magic
		});
	});
});

		
		
</script>



<div id="person" class="ppl spc"></div>
<div id="blog-wrp">
	
</div>
