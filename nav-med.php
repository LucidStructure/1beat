<?php
	$o = isset($_GET['o']) ? $_GET['o'] : FALSE;
?>
<div class="filter medias">
    <ul>
      <li><a href="/media/photos" <?php if($o == 'photos') { echo "class='active'"; }?>>Photos</a></li>
      <li><a href="/media/videos" <?php if($o == 'videos') { echo "class='active'"; }?>>Videos</a></li>
      <li><a href="/media/music" <?php if($o == 'music') { echo "class='active'"; }?>>Music</a></li>
    </ul>
     <?php if($o == 'videos') {
      echo "<p> Videos for OneBeat 2012 and 2013 by Temujin Doran. </p>";
      }
      else if($o == 'photos') {
      echo "<p> Photographs of OneBeat 2012 and 2013 by <a href='http://hannahdevereux.com'>Hannah Devereux</a> </p>"; 
      echo "<p class='l'> click on photos below to view slideshow";
      }
    ?>
</div>
