<?php
	$p = isset($_GET['p']) ? $_GET['p'] : FALSE;
	$o = isset($_GET['o']) ? $_GET['o'] : FALSE;
?>
<!DOCTYPE html>
<html>
<head>

<!-- 	<meta name="viewport" content="width=device-width, initial-scale=1"> -->

	<link type="text/css" rel="stylesheet" href="http://dev.1beat.org/ui/mode.css" />
	<link type="text/css" rel="stylesheet" href="http://1beat.org/ui/mode-luci.css" />
	<link href="http://fonts.googleapis.com/css?family=Signika:400,600" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Abel" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script type="text/javascript" src="http://dev.1beat.org/ui/j/luci.js"></script>
	<script type="text/javascript" src="http://1beat.org/ui/j/sc-api.js"></script>
	<script type="text/javascript" src="https://w.soundcloud.com/player/api.js"></script>

	<title>OneBeat</title>

	<meta property="og:image" content="https://s3-us-west-2.amazonaws.com/1beast/pdfthumb.jpg" />
	<meta property="og:url" content="http://www.1beat.org/" />
	<meta property="og:title" content="OneBeat" />
	<meta property="og:site_name" content="OneBeat" />
	<meta name="keywords" content="Found Sound Nation 1Beat Musician Residency International Department of State" />
	<meta name="author" content="Lucid Structure">

	<script>

	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-28351028-1', 'auto');
	  ga('send', 'pageview');

	</script>

</head>
<body>
	<div id="ovr"></div>
	<div id="hdr-wrp" class="hdr-wrp">
		<div class="twtl-wrp">
			<div class="hdr">
				<img src="https://s3-us-west-2.amazonaws.com/1beast/2015/1blogo15.png" height="36" width="36" />
				<a href="http://1beat.org/">ONEBEAT</a>
				<ul>
					<li><a href="/about/" <?php if($p == 'about') { echo "class='active'"; }?>>About</a></li>
					<li><a href="/community/blog" <?php if($p == 'community') { echo "class='active'"; }?>>Community</a></li>
					<li><a href="/media/videos" <?php if($p == 'media') { echo "class='active'"; }?>>Media</a></li>
				</ul>
			</div>
			<!-- <a href="http://1beat.org/about/apply#apply" class="twtl">
				<p>Apply</p>
			</a> -->
		</div>
	</div>
	<?php if($p == 'about' || $o) { echo "<div class='monde'></div>"; }
