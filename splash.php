<script type="text/javascript" src="ui/j/splash.js"></script>

<!-- <div class="banner">
	<h3>Meet the 2014 Fellows!</h3>
</div> -->
<div class="splash">
<!-- slideshow - théomode -->
<div class="sss_f">
	<div class="sli">
		<!-- <div class="s f one" name="one">
			<div class="wrd">
				<h1>OneBeat 2015</h1>
				<p>Call for application open Jan 5 - Feb 13</p>
				<a href="http://1beat.org/about/apply#apply" class="watch">Apply now</a>
			</div>
		</div> -->
		<div class="s f two" name="two">
			<div class="wrd">
				<h1>A musical journey like no other</h1>
				<p>OneBeat 2014 brought together 25 musicians from 17 countries for a month-long residency and tour</p>
				<a href="https://www.youtube.com/watch?v=k_KG1bmuMjg&index=1&list=PLkLzOTNw7TRTuOSUw4ucD4dsSb1lcjd6U" class="watch">Watch Films</a>
			</div>
		</div>
		<div class="s three" name="three">
			<div class="wrd">
				<h1>music collaboration across the globe and around the block</h1>
				<a href="/about/mission" class="watch">Learn more</a>
			</div>
		</div>
		<div class="s four l" name="four">
			<div class="wrd">
				<h1>OneBeat interviews</h1>
				<p>'14 Fellows share their experiences and talk about their ideas for innovative community music projects</p>
				<a href="https://www.youtube.com/watch?v=EUPwTq3wtAI&list=PLkLzOTNw7TRQ1YLpu7xrJnKyDB6LT2lfO&index=2" class="watch">Watch now</a>
			</div>
		</div>
		<!-- <div class="s five l" name="five">
			<div class="wrd">
				<h1>Sunu Kaddu in Senegal</h1>
				<p>Fellow PPS leads new youth hip-hop community education initiative</p>
				<a href="/community/people/ppl/#PPS" class="watch">Watch now</a>
			</div>
		</div> -->
	</div>
	<div class="crc">
		<div class="c two" name="two"></div>
		<div class="c three" name="three"></div>
		<div class="c four" name="four"></div>
	<!-- 	<div class="c five l" name="five"></div> -->
	</div>
	<div class="wrp">
		<div class="arw l"></div>
		<div class="arw r"></div>
	</div>
</div>
	<div class="statement splash">
		<h4>OneBeat is a music exchange and incubator for music-based social entrepreneurship, where innovative musicians from around the world launch collaborative projects designed to make a positive impact on local and global communities.  An initiative of the U.S. State Department's Bureau of Educational and Cultural Affairs, OneBeat is cultivating a groundbreaking international network of leading artistic, technological, and social innovators in music.</h4>
	</div>
	<div class="social">
		<ul>
			<li class="l"><a href="https://www.facebook.com/1beatmusic" class="fb"></a><span><a href="https://www.facebook.com/1beatmusic">Join On Facebook</a></span></li>
			<li><a href="https://twitter.com/1beatmusic" class="twt"></a><span><a href="https://twitter.com/1beatmusic">Follow On Twitter</a></span></li>
		</ul>
	</div>
	<a href="/about/mission" class="more u">
		read more	
	</a>
	<div class="dv"></div>
	<div id="blog-splash" class="blog cf"></div>
	<!-- <div class="dv2"></div>
	<div id="videos-splash" class="videos cf"></div> -->
	<div class="dv2"></div>
		
	<div class="sponsors splash">
		<h2>Partners &amp; Supporters</h2>
		<div class="big cf">
			<a href="http://exchanges.state.gov/" class="logo gov">
				<h4>a program of</h4>
			</a>
			<a href="http://foundsoundnation.org/curations/" class="logo fsn">
				<h4>produced by</h4>
			</a>
		</div>
		<div class="med cf">
			<a href="http://bangonacan.org/" class="logo boc">
				<h4>administered by</h4>
			</a>
			<a href="http://www.rsclark.org/" class="logo rsc">
				<h4>additional major support provided by</h4>
			</a>
		</div>
		<div class="sml cf">
			<h4>past sponsors and supporters</h4>
			<div class="one cf">
				<a href="http://montalvoarts.org/" class="logo-p mont"></a>
				<a href="http://516arts.org/" class="logo-p fos"></a>
				<a href="https://arcosanti.org/" class="logo-p arco"></a>
				<a href="http://artsharela.org/" class="logo-p art-shr"></a>
			</div>
			<div class="two cf">
				<a href="http://ghostranch.org/" class="logo-p ghost-ranch"></a>
				<a href="https://www.nhccfoundation.org/" class="logo-p nhccf"></a>
				<a href="" class="logo-p yards"></a>
				<a href="https://www.ableton.com/" class="logo-p abl"></a>
			</div>
			<div class="three cf">
				<a href="http://www.pelusomicrophonelab.com/" class="logo-p pel"></a>
				<a href="http://worldup.org/" class="logo-p world-up"></a>
				<a href="http://to.be/" class="logo-p tobe"></a>
				<a href="http://www.glyphtech.com/" class="logo-p glyph"></a>
				<a href="http://www.daddario.com/DaddarioSplash.Page?ActiveID=1740" class="logo-p dard"></a>
				<a href="http://www.dubspot.com/" class="logo-p dspot"></a>
			</div>
		</div>
	</div>




</div>