<div class="content program">
	<h2>Program</h2>
	<div class="banners">
		<a href="http://foundsoundnation.org/curations/onebeat-2012/"><img src="https://s3-us-west-2.amazonaws.com/1beast/2015/Archives-2012.jpg" /></a>
		<a href="http://foundsoundnation.org/curations/onebeat-2013/"><img src="https://s3-us-west-2.amazonaws.com/1beast/2015/Archives-2013.jpg" /></a>
		<a href="http://1beat.org/festival/?p=festival#videos"><img src="https://s3-us-west-2.amazonaws.com/1beast/2015/Archives-2014.jpg" /></a>
	</div>
	<h2>OneBeat 2015 Dates: October 12 - November 11</h2>
	<p><span><strong>Opening Residency:</strong></span>
	OneBeat begins with a two-week musical residency where OneBeat Fellows will collaborate to 
	invent new musical works, record and produce tracks in custom-built mobile studios, design public 
	engagement workshops, and meet with social entrepreneurs. The Residency is a time for Fellows to 
	listen to one another's musical voices and to weave together their interests, 
	histories, and skills into unique, original works.</p>

	<p><span><strong>Tour:</strong></span> After the residency, OneBeat hits the road for a two-week tour featuring performances, 
	youth workshops, and public music-making events. Past events have included an art installation and music 
	festival in an abandoned train factory, university panel discussions and workshops, interactive street studios, 
	and much more. Public events & workshops for 2015’s program will be announced in the coming months.</p>
	

	<p><span><strong>One Beat, Three Approaches:</strong></span> We encourage OneBeat Fellows to work together, across borders of 
	musical traditions and backgrounds, in three complementary approaches that we call Analog, Digital, and Social. 
	Analog refers to creating music the 'old fashioned' way, getting together to explore new music with instruments 
	and voices. In the Digital approach, OneBeat fellows make use of the latest digital technology, and work with music 
	and video software programs. Using the Social approach, OneBeat fellows develop music-driven social enterprises, 
	ranging from youth hip-hop education centers to music therapy practices to socially-engaged record labels. </p>
	
	<p><span><strong>The OneBeat Philosophy:</strong></span> OneBeat is designed with the belief that the creation of high-quality 
	original music is an ideal vehicle for crossing cultural and political divides. This creative process 
	exemplifies how people from drastically divergent musical backgrounds, with diverse training, conceptions 
	of music, and aesthetics, can negotiate differences and find an interplay of cultures that maintains the 
	integrity of each tradition.  At OneBeat we encourage musicians to listen deeply to each others' musical voices, 
	and to create work that values the complexity and idiosyncrasies of each tradition, creating risky, 
	wholly unique musical works. This type of egalitarian, cross-cultural interaction continues in our 
	discussions of social issues, as we find ways to use music to catalyze positive change in our communities 
	and internationally.</p>
</div>