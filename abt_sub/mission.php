<script>
	function monde() { 
	// 	$('.hdr-wrp').addClass('world') 
	// }
	// <?php $p = isset($_GET['p']) ? $_GET['p'] : FALSE;
	// 	if ($p == 'about'){
	// 		echo 'monde()';
	// 	}
	// ?>
</script>
<div class="content mission">
	<h2>Mission</h2>
	<div class="v_about">
		<iframe width="550" height="309" src="//www.youtube.com/embed/k_KG1bmuMjg?list=PLkLzOTNw7TRTuOSUw4ucD4dsSb1lcjd6U" frameborder="0" allowfullscreen></iframe></div>
	<p>OneBeat is an incubator for music-based social entrepreneurship where innovative musicians from around the world launch collaborative 
	projects designed to make a positive impact on local and global communities. Now in its third year, OneBeat is cultivating a groundbreaking 
	international network of leading artistic, technological, and social innovators in music. An initiative of the U.S. State Department's Bureau 
	of Educational and Cultural Affairs in collaboration with the groundbreaking New York-based music organization Bang on a Can's 
	Found Sound Nation, OneBeat employs collaborative original music as a potent new form of cultural diplomacy. 
	</p>
	<p>OneBeat brings musicians (ages 19-35) from around the world to the U.S. for one month each fall to collaboratively write, 
	produce, and perform original music, and develop strategies for arts-based social engagement. OneBeat begins with an opening residency, 
	when Fellows collaborate to create original material, record new musical ideas, and incubate their projects.  
	OneBeat fellows then go on tour, performing for a wide array of American audiences, collaborating with local musicians, 
	and leading workshops with youth. In a closing residency, each OneBeat musician sets out their plans for the future, 
	developing projects in their home countries linked to a mutually-reinforcing network of music-driven social enterprises.</p>
	<p>OneBeat is a musical journey like no other. It is a chance for adventurous musicians from an incredible diversity of 
	traditions to seek common ground, create new musical combinations, push the boundaries of music technology, and find ways 
	to involve all members of society in the process of musical creativity. OneBeat endeavors to be the nexus of a new way 
	of thinking about how music can help us collectively build healthy communities, prosperous societies, and a more peaceful 
	world. </p>	
</div>
