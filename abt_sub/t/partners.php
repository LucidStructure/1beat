<div class="content partner">
	<h2>partners</h2>
	<ul class="filter">
		<li onclick="partners()" id="f-partners">Partners</li> |
		<li onclick="supporters()" id="f-supporters">Supporters</li> |
		<li onclick="sponsors()" id="f-sponsors">Sponsors</li>
	</ul>
	<div class="wrp">
		<div id="partner"></div>
		<div id="supporter"></div>
		<div id="sponsor"></div>
	</div>
</div>
<script>
	function partners() {
		var hash = "partners";
		window.location.hash = hash;
		window.location.reload();
	}
	function supporters() {
		var hash = "supporters";
		window.location.hash = hash;
		window.location.reload();
	}
	function sponsors() {
		var hash = "sponsors";
		window.location.hash = hash;
		window.location.reload();
	}
	var type = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
	var output 
	if(type == 'partners') {
		document.getElementById("f-partners").setAttribute("class", "a");
		output = '<div class="info"> \
					<h3><span>A Program Of:</span> ECA</h3> \
					<p>OneBeat is an initiative of the U.S. Department of State’s Bureau of Educational and Cultural Affairs. \
					The Bureau supports U.S. foreign policy by fostering mutual understanding between the people of the United States \
					and the people of other countries through academic, cultural, sports, and professional exchanges that engage women, \
					youth, underserved communities, educators, artists, athletes, and rising leaders in the U.S. and more than 160 countries.</p> \
					</div> \
					<div class="info"> \
						<h3><span>Produced by:</span> Found Sound Nation</h3> \
						<p>OneBeat is produced by Found Sound Nation, an eclectic group of musicians, producers, and \
						artists who have a strong sense of social engagement. Working with people across the globe, \
						from schools to prisons, from young to old, and partnering with local youth, social organizations, \
						music festivals, and artists across all disciplines, Found Sound Nation uses the expressive \
						power of music collaboration to unlock the creative potential of youth, and build bridges between \
						communities separated by cultures, economic disparities, and geography. Found Sound Nation \
						is the social engagement wing of Bang on a Can, the groundbreaking organization for new music.</p> \
					</div> \
					<div class="info"> \
						<h3><span>Administered by:</span> Bang on a Can</h3> \
						<p>Bang on a Can is dedicated to making music new. Since its first Marathon concert in 1987, \
						Bang on a Can has been creating an international community dedicated to innovative music, wherever \
						it is found. With adventurous programs, it commissions new composers, performs, presents, and records \
						new work, develops new audiences, and educates the musicians of the future. Bang on a Can is building a \
						world in which powerful new musical ideas flow freely across all genres and borders. </p> \
					</div> \
					<div class="info"> \
						<h3><span>Additional Major Support Provided by:</span> Robert Sterling Clark Foundation</h3> \
						<p>The Robert Sterling Clark Foundation was incorporated in 1952 and has since provided financial \
						assistance to a wide variety of organizations. At present, they are concentrating their resources in the \
						following three fields: Promoting International Arts Engagement, Protecting Reproductive Rights, \
						Improving the Performance of Public Institutions in New York. The objective of the Foundation’s \
						international program is to promote global understanding through the arts. The Foundation has \
						initial plans to support international engagement involving U.S. visual and performing arts organizations \
						and their counterparts primarily in Africa, Latin America and the Middle East.</p> \
					</div> \
				</div>';

		document.getElementById("partner").innerHTML+=output;


	} else if(type == 'supporters') {


		document.getElementById("supporter").innerHTML+=output;
	} else if(type == 'sponsors') {


		document.getElementById("sponsor").innerHTML+=output;
	}
</script>