	<div class="content press">
	<h2>press</h2>
	<!-- <ul class="filter">
		<li onclick="tTR()" id="f-2013">2013</li> |
		<li onclick="tTW()" id="f-2012">2012</li>
	</ul> -->
	<div class="wrp">
		<div id="twl"></div>
		<div id="thr"></div>
	</div>
</div>
<script>
	// function tTR() {
	// 	var hash = "2013";
	// 	window.location.hash = hash;
	// 	window.location.reload();
	// }
	// function tTW() {
	// 	var hash = "2012";
	// 	window.location.hash = hash;
	// 	window.location.reload();
	// }
	// var year = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);

	var	output = '<h2 class="press">2013</h2> \
			<ul class="press"> \
			<li class="info"> \
				<ul> \
					<li>The Rumpus |</li> \
					<li><a href="http://therumpus.net/2013/10/onebeat-finale-this-weekend-in-brooklyn-new-york/">OneBeat Finale this weekend in Brooklyn, New York </a>|</li> \
					<li>10.04.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Xinhua News Agency |</li> \
					<li><a href="http://news.xinhuanet.com/newmedia/2013-10/06/c_117604284.htm">OneBeat Feature </a>|</li> \
					<li>10.06.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Voice of Russia |</li> \
					<li><a href="http://voiceofrussia.com/us/2013_10_05/Russian-beat-box-extraordinaire-visits-NYC-6010/">Russian beat box extraodinaire visits NYC </a>|</li> \
					<li>10.05.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>The Aquarian Weekly |</li> \
					<li><a href="http://www.theaquarian.com/2013/10/03/onebeat-around-the-world/">OneBeat: Around the World </a>|</li> \
					<li>10.03.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>El Universal |</li> \
					<li><a href="http://www.eluniversal.com/arte-y-entretenimiento/131003/maracas-y-tambores-como-pasaporte">Maracas y Tambores come pasaporte </a>|</li> \
					<li>10.03.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Time Out New York |</li> \
					<li><a href="http://www.timeout.com/newyork/music/burning-spear-onebeat">Burning Spear and OneBeat </a>|</li> \
					<li>09.27.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Flavorpill |</li> \
					<li><a href="http://beta.flavorpill.com/events/opening-night-burning-spear-slash-onebeat">Opening Night: Burning Spear / OneBeat (Event Preview)  </a>|</li> \
					<li>10.01.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>The Daily Tar Heel  |</li> \
					<li><a href="http://www.dailytarheel.com/article/2013/09/onebeat-0930">Musicians come together for OneBeat </a>|</li> \
					<li>09.30.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>The Herald Sun |</li> \
					<li><a href="http://www.heraldsun.com/news/showcase/x249846502/OneBeat-to-give-workshops-perform">OneBeat to give workshops, perform </a>|</li> \
					<li>09.27.13 </li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Chapel Hill Magazine |</li> \
					<li><a href="http://www.chapelhillmagazine.com/events/one-beat-show-feat-lila-and-sacrificial-poets/">OneBeat (Event Preview) </a>|</li> \
					<li>09.26.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Indy Week |</li> \
					<li><a href="http://www.indyweek.com/indyweek/the-week-in-music-sept-25-oct-2-2013/Content?oid=3727123">OneBeat (Event Preview) </a>|</li> \
					<li>09.25.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Tiny Mix Tapes |</li> \
					<li><a href="http://www.tinymixtapes.com/chocolate-grinder/listen-onebeat-onebeat-mixtape-2012">OneBeat: Mixtape 2012 </a>|</li> \
					<li>08.15.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Durham News Observer |</li> \
					<li><a href="http://www.newsobserver.com/2013/09/26/3226867/durham-musician-promotes-art-activism.html">Durham musician promotes Art + Activism Festival... </a>|</li> \
					<li>09.26.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>The Post and Courier |</li> \
					<li><a href="http://www.postandcourier.com/article/20130922/PC1610/130929763/1002/onebeat-x2019-s-x2018-musical-journey-x2019-coming-to-lowcountry">OneBeats "musical journey" coming to Lowcountry </a>|</li> \
					<li>09.22.13 </li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>WMFE |</li> \
					<li><a href="http://www.wmfe.org/site/News2?page=NewsArticle&id=15026&news_iv_ctrl=1041">World Musicians Kick off Cultural Diplomacy Tour in Orlando </a>|</li> \
					<li>09.20.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Daytona News-Journal |</li> \
					<li><a href="http://www.news-journalonline.com/article/20130920/ENT/130929989/1064?p=1&tc=pg">OneBeat taps into world groove at Atlantic Center </a>|</li> \
					<li>09.20.13 </li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Splinters & Candy |</li> \
					<li><a href="http://splintersandcandy.com/?p=9565">Various Artists -- OneBeat 2013 (mixtape review) </a>|</li> \
					<li>09.19.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>The Orlando Sentinel |</li> \
					<li><a href="http://www.orlandosentinel.com/entertainment/music/os-jim-abbott-one-beat-orlando-concert-20130919,0,7627254.column">OneBeat speaks language of music </a>|</li> \
					<li>09.19.13 </li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Orlando Weekly |</li> \
					<li><a href="http://photos.orlandoweekly.com/index.php/meet-the-unfathomably-talented-musicians-and-artists-of-onebeat-2013/#20">Meet the unfathomably talented musicians and artists of On... </a>|</li> \
					<li>09.19.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Charleston City Paper |</li> \
					<li><a href="http://orlandoweekly.com/music/onebeat-returns-to-shake-up-the-definition-of-world-music-1.1553960?pgno=1">OneBeat 2013: Musical Collaboration Across The World... </a>|</li> \
					<li>09.18.13 </li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Orlando Weekly |</li> \
					<li><a href="http://orlandoweekly.com/music/onebeat-returns-to-shake-up-the-definition-of-world-music-1.1553960?pgno=1">OneBeat returns to shake up the definition of world music </a>|</li> \
					<li>09.18.13 </li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Charleston City Paper |<li> \
					<li><a href="http://www.charlestoncitypaper.com/FeedbackFile/archives/2013/08/26/bill-carson-and-uncle-sam-bring-a-team-of-international-musicians-to-the-holy-city">Bill Carson and Uncle Sam bring a team of internation... </a>|</li> \
					<li>08.26.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Miami Herald |</li> \
					<li><a href="http://www.miamiherald.com/2013/09/01/3596113/florida-onebeat-performance-in.html">Florida: OneBeat Performance in New Smyrna Beach, Orlando </a>|</li> \
					<li>09.01.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>The NSB Observer |</li> \
					<li><a href="http://www.nsbobserver.com/2nd-annual-onebeat-residency-brings-a-world-of-music-to-central-florida/">2nd Annual OneBeat Residency Brings a World of Music... </a>|</li> \
					<li>09.10.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Daytona News-Journal |</li> \
					<li><a href="http://www.news-journalonline.com/article/20130913/ENT/130919790/1064?Title=OneBeat-concerts-gather-25-musicians-from-16-countries">OneBeat concerts gather 25 musicians from... </a>|</li> \
					<li>09.13.13</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Niaje |</li> \
					<li><a href="http://niaje.com/blog/kenyan-lady-selected-to-represent-kenya-in-onebeat-2013-tour/">Kenyan Lady Selected to Represent Kenya in OneBeat 2013 Tour </a>|</li> \
					<li>09.09.13</li> \
				</ul> \
			</li> \
		</ul> \
		<h2 class="press">2012</h2>\
		<ul class="press"> \
			<li class="info"> \
				<ul> \
					<li>The New York Times |</li> \
					<li><a href="http://www.nytimes.com/2012/10/04/arts/music/us-onebeat-program-melds-32-musicians-from-21-countries.html?_r=3&">A United Nations of Music </a>|</li> \
					<li>10.03.12 </li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>The Green Point News |</li> \
					<li><a href="http://www.greenpointnews.com/entertainment/4929/autumn-bowl-to-light-up-greenpoint-waterfront-this-month">Autumn Bowl to Light Up Greenpoint Waterfront </a>|</li> \
					<li>10.04.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>OneBeat Exchange Tonight at Atlas |</li> \
					<li><a href="http://www.thehillishome.com/2012/10/onebeat-exchange-tonight-at-the-atlas/">OneBeat Exchange Tonight at Atlas </a>|</li> \
					<li>10.03.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>The Roanoke Times |</li> \
					<li><a href="http://www.roanoke.com/extra/wb/314561">A World of Music Visits the Valley </a>|</li> \
					<li>09.27.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>My Hometown News |</li> \
					<li><a href="http://www.myhometownnews.net/index.php?id=96947">OneBeat musicians from around the world collaborate... </a>|</li> \
					<li>09.21.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Charleston Post and Courier |</li> \
					<li><a href="http://www.rockpaperscissors.biz/index.cfm/fuseaction/current.articles_detail/project_id/620/article_id/19212.cfm">OneBeat, unique international music project... </a>|</li> \
					<li>09.23.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Next Three Days |</li> \
					<li><a href="http://www.nextthreedays.com/FeaturedEventDetails.cfm?E=186013">OneBeat (Concert Preview) </a>|</li> \
					<li>09.27.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Orlando Weekly |</li> \
					<li><a href="http://calendar.orlandoweekly.com/Events/e26764/OneBeat">OneBeat (Concert Preview) </a>|</li> \
					<li>09.25.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Daytona News-Journal |</li> \
					<li><a href="http://www.news-journalonline.com/article/20120918/ENT/309189966/1064?Title=World-beat-International-musicians-on-a-mission-in-New-Smyrna-Beach">World beat: International Musicians on a mission... </a>|</li> \
					<li>09.18.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Charleston City Paper |</li> \
					<li><a href="http://www.charlestoncitypaper.com/FeedbackFile/archives/2012/09/17/onebeat-brings-musicians-from-21-countries-to-chucktown">OneBeat brings musicians 21 countries to... </a>|</li> \
					<li>09.17.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Imperial Valley News |</li> \
					<li><a href="http://www.imperialvalleynews.com/index.php/news/national-news/1557-world-s-musicians-come-together-for-onebeat-tour.html">World Musicians Come Together for "OneBeat Tour" </a>|</li> \
					<li>09.05.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Mountain Xpress |</li> \
					<li><a href="http://www.mountainx.com/article/45773/OneBeat">OneBeat (Concert Preview) </a>|</li> \
					<li>09.25.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Arts Diplomacy Network |</li> \
					<li><a href="http://artsdiplomacy.com/2012/08/24/onebeat-musicians-for-global-innovation/">OneBeat: Musicians for for Global Innovation </a>|</li> \
					<li>08.24.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>GLM News |</li> \
					<li><a href="http://blog.greatlakesmix.com/2012/09/kenyamuthoni-dq-flies-out-for-a-major-world-concert/">Muthoni Ndonga known by her street moniker Drummer Queen is j... </a>|</li> \
					<li>09.07.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Charleston City Paper |</li> \
					<li><a href="http://www.charlestoncitypaper.com/charleston/onebeat/Event?oid=4158277">OneBeat (Concert Preview) </a>|</li> \
					<li>09.26.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>eNews Channels |</li> \
					<li><a href="http://enewschannels.com/2012/09/05/enc15265_155943.php">World Musicians Come Together for "OneBeat Tour </a>|</li> \
					<li>09.05.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Philly Fun Guide |</li> \
					<li><a href="http://www.phillyfunguide.com/event/detail/440872005">OneBeat (Concert Preview) </a>|</li> \
					<li>10.04.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>POP! TECH |</li> \
					<li><a href="http://poptech.org/blog/interview_social_innovation_fellow_chris_marianetti_on_found_sound_nations_new_initiative_with_the_state_department">Interview: Social Innovation Fellow Chris Marianetti on Found... </a>|</li> \
					<li>01.20.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Kadmus Arts |</li> \
					<li><a href="http://kadmusarts.com/podcasts/?p=2388">Jeremy Thal: Found Sound Nation </a>|</li> \
					<li>10.09.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>Orlando Sentinel |</li> \
					<li><a href="http://findlocal.orlandosentinel.com/listings/onebeat-orlando">OneBeat (Concert Preview) </a>|</li> \
					<li>09.27.12</li> \
				</ul> \
			</li> \
			<li class="info"> \
				<ul> \
					<li>New York Daily News |</li> \
					<li><a href="http://events.nydailynews.com/brooklyn_ny/events/show/283308347-onebeat">OneBeat (Concert Preview) </a>|</li> \
					<li>09.27.12</li> \
				</ul> \
			</li> \
		</ul>';
		document.getElementById("twl").innerHTML+=output;
	

</script>
