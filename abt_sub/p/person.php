<script>
$(document).ready(function() {
	var user = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
	var str = user
	var output = ''
		$.getJSON('../../../ui/j/json/people.json', function(data) {
			for(var j in data.people) {
				
				if(data.people[j].name.replace(/ /g,'').toLowerCase() == str.replace(/ /g,'').toLowerCase()) {
					if(data.people[j].type == "Fellow") {
						output += '<div class="content fellow person">'
						output += '<h2>Fellows</h2>'
						output += '<ul class="filter"> \
										<li id="f-2014"><a href="/about/fellows/#2014">2014</a></li> | \
										<li id="f-2013"><a href="/about/fellows/#2013">2013</a></li> | \
										<li id="f-2012"><a href="/about/fellows/#2012">2012</a></li> \
									</ul>';
					} else if(data.people[j].type == "Staff") {
						output += '<div class="content team person">'
						output += '<h2>Team</h2>'
						output += '<ul class="filter"> \
										<li id="f-2014"><a href="/about/staff/#2014">2014</a></li> | \
										<li id="f-2013"><a href="/about/staff/#2013">2013</a></li> | \
										<li id="f-2012"><a href="/about/staff/#2012">2012</a></li> | \
										<li id="f-YR"><a href="/about/staff/#core">Core Staff</a></li> \
									</ul>';

					} else if(data.people[j].type == "Collaborating Artist") {
						output += '<div class="content collab person">'
						output += '<h2>Collaborators</h2>'
						output += '<ul class="filter"> \
										<li id="f-2013"><a href="/about/collaborators/#2013">2013</a></li> | \
										<li id="f-2012"><a href="/about/collaborators/#2012">2012</a></li> \
									</ul>';
					} else if(data.people[j].type == "Extended") {
						output += '<div class="content extend person">'
						output += '<h2>Extended</h2>'
					}
					//output += '<ul class="filter"><li><span>Year Round</span>  |</li><li><span>2014</span>  |</li><li><span>2013</span>  |</li><li><span>2012</span></li></ul>'
					output += '<div class="person">'
					output += '<div clas="img-wrp">'
					if(data.people[j].pic != undefined) {
						output += '<img src="'+data.people[j].pic+'" />'	
					}
					
					output += '</div>'

					if(data.people[j].credit != undefined) {
						output += '<div class="credit">' + data.people[j].credit + '</div>'
					}
					
					output += '<h2>'+data.people[j].name+'</h2><h3>OneBeat ' 
					if(data.people[j].year != undefined && data.people[j].year != "year round") {
						output += data.people[j].year + ' '
					}
					if(data.people[j].type != undefined) {
						output += data.people[j].type 
					}
					output += '</h3><h3>'
					if(data.people[j].instrument != undefined) {
						output +=  data.people[j].instrument 
					} 
					if(data.people[j].city != undefined) {
						output += ' | ' + data.people[j].city + ', '
						//NESTED FOR THE BAR ||||||
						if(data.people[j].country != undefined) {
							output += data.people[j].country 
						}
					} else if(data.people[j].country != undefined) {
							output += ' | ' + data.people[j].country 
					}
					output += '</h3>'
					if(data.people[j].bio != undefined) {
						output += '<div class="bio">'+data.people[j].bio + '</div><'
					}

					output += '/div><div class="social"><ul>'
					if(data.people[j].website != undefined) {
						output += '<li><a href="'+data.people[j].website+'" target="_blank">website</a> |</li>'
		 			}

					if(data.people[j].twitter != undefined) {
						output += '<li><a href="'+data.people[j].twitter+'" target="_blank">twitter</a> |</li>'
		 			}
					if(data.people[j].soundcloud != undefined) {
						output += '<li><a href="'+data.people[j].soundcloud+'" target="_blank">soundcloud</a> |</li>'
		 			}
					output += '</ul></div></div></div></div>'

					if(data.people[j].feat != undefined) {
					output += '<h2 class="fp">Featured Media</h2> \
								<div class="dv"></div>';
					}
					
					document.getElementById("person").innerHTML+=output;

					if(data.people[j].feat != undefined) {
						var m = '<div class="blog post"><div class="info"><div class="video">'
							m+= data.people[j].feat
							m+= '</div></div></div>'
							document.getElementById("blog-wrp").innerHTML+=m;
					}
					

				}
			}
	});
});

		
		
</script>



<div id="person"></div>

<div id="blog-wrp">
	
</div>
