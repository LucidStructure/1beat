<script>
	// function monde() { 
	// 	$('.hdr-wrp').addClass('world') 
	// }
	// <?php $p = isset($_GET['p']) ? $_GET['p'] : FALSE;
	// 	if ($p == 'about'){
	// 		echo 'monde()';
	// 	}
	// ?>
</script>
<div class="content quotes">
	<h2>quotes</h2>
	<div class="quote">
		<h3>"OneBeat changed my life and my mind. It let me know that people can be so close to each other,
		and that music has a strong and special power in making the world a better one."</h3>
		<h4>- <a href="/about/fellows/p/#WeiWei">Wei Wei</a>, 2012 OneBeat Fellow from Beijing, China</h4>
	</div>
	<div class="quote">
		<h3>"OneBeat is the program that realizes the dream of any musician in the world: to develop their musical talent and mix it with other cultures. 
		It teaches you important things that complement your musical career, and gives you the opportunity to have friends from all parts of the world."</h3>
		<h4>- <a href="/about/fellows/p/#ManuelRangel">Manuel Rangel</a>, 2013 Fellow from Caracas, Venezuela</h4>
	</div>

	<div class="quote">
		<h3>"I see OneBeat as a breakthrough in music diplomacy...whilst there's been a rich heritage of music diplomacy 
		going back to the 19th century, it used to mean is that countries would arrange for their star musicians going overseas 
		and perform.  But OneBeat is different because it isn't about America sending Americans overseas, 
		it's about bringing musicians to this country to meet each other, to engage with Americans here, 
		and to produce a musical conversation, and it's that sense where the whole program represents the 
		United States listening to the world in an important way...it's about being relevant in more ways 
		than just showing just how fantastic the culture that originated in the United States is.  
		We can be relevant by facilitating reconciliation, and I think that's hard to beat."</h3>
		<h4>- <a href="http://annenberg.usc.edu/Faculty/Communication%20and%20Journalism/CullN.aspx">Nicholas Cull</a>, Professor of Public Diplomacy at USC Annenberg</h4>
	</div>

	<div class="quote">
		<h3>"World music in its truest sense"</h3>
		<h4>- <a href="http://www.nytimes.com/2012/10/04/arts/music/us-onebeat-program-melds-32-musicians-from-21-countries.html?_r=4&">The New York Times</a>, October 3, 2012</h4>
	</div>
	<div class="quote">
		<h3>"Saying that this is a musical collaboration and engagement for me feels like it doesn't encompass everything that OneBeat is. 
		It's definitely something more than just music and art. For me it feels like a way of life." </h3>
		<h4>- <a href="/about/fellows/p/#SivaDarbuka">Siva Darbuka</a>, 2013 Fellow from Chennai, India </h4>
	</div>

	<div class="quote">
		<h3>"Meeting all of these 25 people who have that same belief and that same mission, that music has the power to change, 
		that music has the power to move people <em>into</em> a position where they are willing to change...that's 
		an incredibly powerful experience."</h3>
		<h4>- <a href="/about/fellows/p/#Kyla-RoseSmith">Kyla Rose Smith</a>, 2014 OneBeat Fellow from Cape Town, South Africa</h4>
	</div>
	<div class="quote">
		<h3>"Some or most who know about Zimbabwe know about what, let's say, commercial media have been saying about Zimbabwe. 
		So they only know about the political struggles we've been going through or our economic situations, 
		but there are many beautiful things in Zimbabwe. I love being able to show this and also learn from other cultures, 
		and then bring those two together."</h3>
		<h4>- <a href="/about/fellows/p/#HopeMasike">Hope Masike</a>,  2014 OneBeat Fellow from Harare, Zimbabwe</h4>
	</div>

	<div class="quote">
		<h3>"OneBeat is one of the greatest initiatives in advancing the meeting of cultures worldwide." </h3>
		<h4>- <a href="/about/collaborator/p/#DaveDouglas">Dave Douglas</a>, OneBeat 2012 Collaborating Artist </h4>
	</div>
	<div class="quote">
		<h3>"Seeing all these women defy borders and take the stage to make music with determination and hope has 
		sparked an urge to try my best to inspire women back home who have talent, 
		but are scared to come out and share it with the world."</h3>
		<h4>- <a href="/about/fellows/p/#KasivaMutua">Kasiva Mutua</a>, 2012 OneBeat Fellow from Beijing, China </h4>
	</div>
	<div class="quote">
		<h3>"I think beside the musical element, this program is also showing us other ways of transforming our 
		countries and transforming our communities to become better places, to become closer together."</h3>
		<h4>- <a href="/about/fellows/p/#BlessingChimanga">Blessing Chimanga</a>, 2014 OneBeat Fellow from Harare, Zimbabwe</h4>
	</div>

	<div class="quote">
		<h3>"The most valuable thing about OneBeat is the connections and interactions that I have built. 
		Now I really feel I have very good friends spread out in the world, and it makes everything feels warmer."</h3>
		<h4>- <a href="/about/fellows/p/#YoumnaSaba">Youmna Saba</a>, 2013 OneBeat Fellow from Nairobi, Kenya </h4>
	</div>
	
</div>
