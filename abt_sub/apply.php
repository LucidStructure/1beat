

<div class="content apply">
	<h2>Apply</h2>


	<ul class="filter">
		<li onclick="aPL()" id="f-aPL">Apply</li> |
		<li onclick="eL()" id="f-eL">Eligibility</li> |
		<li onclick="fAQ()" id="f-fAQ">Frequently Asked Questions</li> 
	</ul>
	<div class="banners">
		<!-- <img src="https://s3-us-west-2.amazonaws.com/1beast/2015/1Beat15-Badge1.jpg" width="550" />
		<img src="https://s3-us-west-2.amazonaws.com/1beast/2015/1Beat15-Badge4.jpg" width="550" /> -->
		
	</div>
	<div class="wrp">
		<div id="el"></div>
		<div id="faq"></div>
		<div id="apl"></div>
	</div>
	
</div>
<script>

	function eL() {
		var hash = "eligibility";
		window.location.hash = hash;
		window.location.reload();
	}
	function fAQ() {
		var hash = "faq";
		window.location.hash = hash;
		window.location.reload();
	}
	function aPL() {
		var hash = "apply";
		window.location.hash = hash;
		window.location.reload();
	}
	var hash = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
	var output 
	if(hash == 'eligibility') {

	document.getElementById("f-eL").setAttribute("class", "a");

	output = '<div class="opt elg"> \
		<h3>Age</h3> \
		<p>OneBeat is open to musicians ages 19-35. Applicants must be 19 or older at the time of application and 35 or younger at the end of the program (October 2015).</p> \
		<h3>Eligible Countries</h3> \
		<p>Armenia, Azerbaijan, Bangladesh, Bolivia, Brazil, Burma, Colombia, Cuba, Cyprus, Dominican Republic, Egypt, Fiji, Haiti, India, Indonesia, Iraq, Jordan, Kazakhstan, Kenya, Kosovo, Kyrgyzstan, Laos, Lebanon, Malaysia, Morocco, Mozambique, Nigeria, Pakistan, Palestinian Territories, Russia, Senegal, Serbia, South Africa, Sri Lanka, Taiwan, Tunisia, Turkey, Ukraine, United States, Venezuela, Zimbabwe</p> \
		<h3>Important Information for Applicants:</h3> \
		<p>Musicians from all backgrounds are encouraged to apply, with or without formal musical training. We welcome music of all genres, including but not limited to: traditional/folk, hip hop, experimental, electronic, jazz, classical, sound design, beat-making, multimedia art, or any combination of these styles. In additional to full-time professional musicians, we also invite adventurous musicians who double as community organizers, instrument builders, writers, videographers, musicologists, educators, storytellers, dancers, shadow-puppeteers, and more.</p> \
		<h3>What you should know about OneBeat:</h3> \
		<ol> \
			<li><strong>Collaboration is central.</strong> OneBeat offers musicians the opportunity to collaborate on new projects and to explore new musical traditions. Rather than showcasing solo talent, OneBeat musicians will work together across stylistic and cultural divides in pursuit of new musical possibilities.</li> \
			<li><strong>OneBeat is socially engaged.</strong> We are looking for musicians who have a strong commitment to their communities and who are seeking new ways to engage in youth development, music education, music therapy, conflict resolution, a global dialogue through music, and other ways of working towards more egalitarian and democratic societies.</li> \
			<li><strong>OneBeat is a FREE program (but not a paid gig).</strong> The OneBeat program covers all Fellows' + "'" + ' travel, transportation, food, and lodging. We also offer a modest stipend. While we are not able to pay performance fees, OneBeat offers Fellows a chance to create new material, establish lifelong musical partnerships and friendships, learn new marketable skills, tour the US, and develop professional connections.</li> \
		</ol> \
		<h3>Audio/Video Application Materials:</h3> \
		<p>Applicants should plan to write about their musical background and interests as well as future projects and goals. Applicants will be asked to submit 3-5 samples of their solo and collaborative work (both audio and video are accepted).</p> \
		<h3>Review Criteria</h3> \
		<ol> \
			<li>Musical Excellence - A high level of performance, composition, improvisational, production and/or technological skill. Ideally OneBeat musicians will be innovating stylistically, lyrically, or technologically within their musical worlds.</li> \
			<li>Collaboration - Applicants' + "'" + ' willingness to reach across cultural and musical divides in creating original music or re-interpreting traditional music, while respecting the essence of each tradition. Applicants should be prepared to try new things musically.</li> \
			<li>Social Engagement - Musicians who have used music to serve their communities or greater societies. This might consist of guiding young people in music education, addressing social or political issues through musical content, reviving dying musical traditions, and more.</li> \
			<li>Age - Applicants must be between 19-35 years old.</li> \
			<li>English Proficiency - Applicants should be able to converse in and understand basic English, as it will be the common language of the OneBeat program.</li> \
			<li>Country of Origin - OneBeat Fellows must currently live full-time in one of the 40 eligible countries and territories. We also seek applicants who have not traveled extensively to the U.S. or have rarely performed in the U.S.</li> \
			<li>Internet Proficiency - Whenever possible, OneBeat Fellows should actively use email and be able to connect to the internet to participate in OneBeat website-based activities.</li> \
		</ol> \
	</div>';

	document.getElementById("el").innerHTML+=output;


	} else if(hash == 'faq') {

	document.getElementById("f-fAQ").setAttribute("class", "a");

	

	output = '<div class="opt faq"> \
		<ol> \
			<li>Who is eligible to apply?</li> \
			<p>See the ELIGIBILITY tab</p> \
			<li>How do musicians apply to become OneBeat Fellows?</li> \
			<p>See the APPLY tab </p> \
			<li>When can musicians apply? When is the application deadline?</li> \
			<p>Applications for the fall 2015 program open on January 5, 2015, and are due by February 14, 2015, 5:00 p.m. EST.</p> \
			<li>Do I have to be a certain age to apply to OneBeat?</li> \
			<p>Applicants must be 19 or older at the time of the application and 35 or younger at the end of the program (October 2015).</p> \
			<li>What are the eligible countries and territories?</li> \
			<p>Armenia, Azerbaijan, Bangladesh, Bolivia, Brazil, Burma, Colombia, Cuba, Cyprus, Dominican Republic, Egypt, \
			Fiji, Haiti, India, Indonesia, Iraq, Jordan, Kazakhstan, Kenya, Kosovo, Kyrgyzstan, Laos, Lebanon, Malaysia, \
			Morocco, Mozambique, Nigeria, Pakistan, Palestinian Territories, Russia, Senegal, Serbia, South Africa, Sri Lanka, \
			Taiwan, Tunisia, Turkey, Ukraine, United States, Venezuela, Zimbabwe</p> \
			<li>Why are certain countries not eligible for OneBeat? Will you make any exceptions?</li> \
			<p>OneBeat 2015 will be focused on musicians from the countries listed above. Eligible countries are determined by the US Department of State, \
			and unfortunately, we arent able to bend the country eligibility. However, there are other programs funded by the US Department of State that you may be eligible for. \
			For more details see: <a href="http://exchanges.state.gov/non-us">http://exchanges.state.gov/non-us.</a></p> \
			<li>Can I apply if I am from one of the eligible countries, but currently living in a non-eligible country?</li> \
			<p>While we are sensitive to the fact many countries have an extensive diaspora, \
			in order to be eligible for OneBeat, applicants must \
			be both a resident and citizen of one of the eligible countries.</p> \
			<li>Can I apply if I am currently a resident of one of the eligible countries, but am originally from elsewhere?</li> \
			<p>Unfortunately you are eligible for OneBeat only if you are both a resident and citizen of an eligible country.</p> \
			<li>Are musical groups able to apply together?</li> \
			<p>OneBeat Fellows must apply individually and will be selected as individuals. \
			However, several members of a group are welcome to apply separately.</p> \
			<li>Are U.S. musicians also eligible?</li> \
			<p>Yes. We anticipate accepting a small number of U.S. musicians as OneBeat 2015 Fellows.</p> \
			<li>What is the selection process? When will artists be identified?</li> \
			<p>All applications will be reviewed by a OneBeat Selection Committee made up of OneBeat staff, U.S. Department of State staff, and accomplished musicians. \
			Selected Fellows will be notified by early May, and will have 2 weeks to confirm their participation in the program.</p> \
			<li>Will artists be chosen from every eligible country and territory?</li> \
			<p>Because of the number of eligible countries and areas (41) and the number of anticipated OneBeat \
			2015 Fellows (20-25) we will not be able to accept Fellows from every country. We do intend to invite Fellows \
			from every world region.</p> \
			<li>How many artists will be selected?</li> \
			<p>Approximately 20-25 participants will be selected this year.</p> \
			<li>Do I have to be a full-time professional musician to apply?</li> \
			<p>No, we encourage advanced students and highly skilled semi-professional musicians to apply. \
			(We understand that music isn'+ "'" + 't always the easiest way to make a living!)</p> \
			<li>Are non-musicians who work in related fields eligible to apply?</li> \
			<p>OneBeat is only open to musicians, although we do encourage musicians to apply who also work in other fields.</p> \
			<li>Do OneBeat applicants have to play a particular genre of music?</li> \
			<p>Musicians from any genre or background are encouraged to apply. We are looking for musicians who have achieved an advanced proficiency in styles and skills such as (but not limited to): folk music, hip hop, \
			electronic music, traditional music, jazz, experimental, classical, sound-art installations, DJing, audio engineering, etc.</p> \
			<li>Do participants need to speak English?</li> \
			<p>Participants should have a basic grasp of English in order to participate in ensembles and creative sessions.</p> \
			<li>What are the costs of the OneBeat program for participants?</li> \
			<p>All costs will be covered for OneBeat Fellows, including travel and accommodations. \
			Fellows will also receive a per diem and a modest honorarium of around US$500.</p> \
			<li>Will OneBeat help secure Visas to enter the United States?</li> \
			<p>Yes, OneBeat Fellows will travel under a <a href="http://j1visa.state.gov">J-1 Visa</a>, a non-immigrant visa for individuals approved to participate in work- and study-based exchange visitor programs.</p> \
			<li>How long will participants stay in the US? What will the participants do while in the U.S?</li> \
			<p>The program will be 26-28 days in length. While in the U.S. the Fellows will participate in a two-week residency, during which they will develop and rehearse original material. They will then go on a 10-14 day tour, during which they will perform the music they developed and rehearsed during the residency period. Throughout the program Fellows will develop and lead creative workshops for youth and community groups.</p> \
			<li>Can I participate in only part of the program?</li> \
			<p>No, OneBeat Fellows must commit to participating in the entire program, which will last approximately 28 days, starting in the fall of 2015.</p> \
			<li>Will there be post-OneBeat activities for OneBeat Fellows after the program in fall 2014?</li> \
			<p>Yes. We encourage OneBeat Fellows to be part of a vibrant and growing OneBeat online community, through which Fellows continue to be able to collaborate on new works and project ideas.</p> \
		</ol> \
	</div>';

	document.getElementById("faq").innerHTML+=output;


	} else if(hash == 'apply') {

	document.getElementById("f-aPL").setAttribute("class", "a");

	

	output = '<h2>OneBeat 2015 Dates: October 12 - November 11</h2> \
				<div class="opt apl"> \
				<p>The OneBeat 2015 Application is now closed. \
				If you would like to be notified when the OneBeat \
				2016 application opens or about other OneBeat opportunities, \
				please sign up for our newsletter.\
					<div class="c mch apl"> \
					</div> \
					If you have questions, please contact <a href="mailto:1beat@foundsoundnation.org">1beat@foundsoundnation.org</a> \
				</p> \
			</div>';


	document.getElementById("apl").innerHTML+=output;


	}

</script>





	
	
	

</div>