<script>
	// function monde() { 
	// 	$('.hdr-wrp').addClass('world') 
	// }
	// <?php $p = isset($_GET['p']) ? $_GET['p'] : FALSE;
	// 	if ($p == 'about'){
	// 		echo 'monde()';
	// 	}
	// ?>
</script>
<div class="content team" >
	<h2>Team</h2>
	<ul class="filter">
		
		<li onclick="tTF()" id="f-2014">2014</li> |
		<li onclick="tTR()" id="f-2013">2013</li> |
		<li onclick="tTW()" id="f-2012">2012</li> |
		<li onclick="tTY()" id="f-YR">Core Staff</li>
	</ul>
	<div class="wrp">
		<div id="twl"></div>
		<div id="thr"></div>
	</div>
	<div class="people cf" id="fellows-wrp"></div>
</div>

<script>
	function tTR() {
		var hash = "2013";
		window.location.hash = hash;
		window.location.reload();
	}
	function tTF() {
		var hash = "2014";
		window.location.hash = hash;
		window.location.reload();
	}
	function tTW() {
		var hash = "2012";
		window.location.hash = hash;
		window.location.reload();
	}
	function tTY() {
		var hash = "core";
		window.location.hash = hash;
		window.location.reload();
	}

	var year = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
	var output 
	if(year == '2012') {
		document.getElementById("f-2012").setAttribute("class", "a");

			//STAFF
			$.getJSON('../../ui/j/json/people.json', function(data) {
				for(var j in data.people) {
					var person = ' '

					var yur = ''
					if(data.people[j].year != undefined) {
						yur = data.people[j].year.toString()
					}
				
					if(data.people[j].type == "Staff" && (yur.indexOf(year) > -1)) {
			 		
					person += '<div class="person">'
					person += '<a href="/about/staff/p/#'+data.people[j].name.replace(/ /g,'')+'">'
					person += '<div class="img-wrpr">'
					person += '<img src="'+data.people[j].pic+'"/></a>'
					person += '</div>'
					person += '<h4>'+data.people[j].name+'</h4>'
					person += '<h5>'

					if(data.people[j].role != undefined) {
						person += data.people[j].role + ' | '
					} 

					if(data.people[j].instrument != undefined) {
						person += data.people[j].instrument
					} 
					// if(data.people[j].country != undefined) {
					// 	person += ' | ' + data.people[j].country
					// }
					person += '</h5></div>'
				}
			 	document.getElementById("fellows-wrp").innerHTML+=person;
			 	person = '';
				}

				person += '<div class="dv t"></div>'

				//COLLABS
				for(var j in data.people) {
					
					if(data.people[j].year != undefined) {
						var yur = ''
						if(data.people[j].year != undefined) {
							yur = data.people[j].year.toString()
						}
						
						if(data.people[j].type == "Collaborating Artist" && (yur.indexOf(year) > -1)) {
				 		
							person += '<div class="person">'
							person += '<a href="/about/collaborators/p/#'+data.people[j].name.replace(/ /g,'')+'">'
							person += '<div class="img-wrpr">'
							person += '<img src="'+data.people[j].pic+'"/></a>'
							person += '</div>'
							person += '<h4>'+data.people[j].name+'</h4>'
							person += '<h5>'
							if(data.people[j].type != undefined) {
								person += data.people[j].type + ' | '
							}						
							if(data.people[j].instrument != undefined) {
								person += data.people[j].instrument
							} 
						
						person += '</h5></div>'
					}
				}
			 	document.getElementById("fellows-wrp").innerHTML+=person;
			 	person = '';
				}
		});

	}else if(year == '2013') {
		document.getElementById("f-2013").setAttribute("class", "a");
	$.getJSON('../../ui/j/json/people.json', function(data) {
				for(var j in data.people) {
					var person = ' '
					var yur = ''
					if(data.people[j].year != undefined) {
						yur  = data.people[j].year.toString()
					}
				
					if(data.people[j].type == "Staff" && (yur.indexOf(year) > -1)) {
						person += '<div class="person">'
						person += '<a href="/about/staff/p/#'+data.people[j].name.replace(/ /g,'')+'">'
						person += '<div class="img-wrpr">'
						person += '<img src="'+data.people[j].pic+'"/></a>'
						person += '</div>'
						person += '<h4>'+data.people[j].name+'</h4>'
						person += '<h5>'

						if(data.people[j].role != undefined) {
							person += data.people[j].role + ' | '
						} 

						if(data.people[j].instrument != undefined) {
							person += data.people[j].instrument
						}
						// if(data.people[j].country != undefined) {
						// 	person += ' | ' + data.people[j].country
						// }
						person += '</h5></div>'
				}
			 	document.getElementById("fellows-wrp").innerHTML+=person;
			 	person = '';
				}
				

				person += '<div class="dv t"></div>'
				
				//COLLABS////
				///
				//
				for(var j in data.people) {

					if(data.people[j].year != undefined) {
						var yur = ''
						if(data.people[j].year != undefined) {
							yur =data.people[j].year.toString()
						}
										

						if(data.people[j].type == "Collaborating Artist" && (yur.indexOf(year) > -1)) {
				 		
						person += '<div class="person">'
						person += '<a href="/about/collaborators/p/#'+data.people[j].name.replace(/ /g,'')+'">'
						person += '<div class="img-wrpr">'
						person += '<img src="'+data.people[j].pic+'"/></a>'
						person += '</div>'
						person += '<h4>'+data.people[j].name+'</h4>'
						person += '<h5>'
						if(data.people[j].type != undefined) {
							person += data.people[j].type + ' | '
						}
						if(data.people[j].instrument != undefined) {
							person += data.people[j].instrument
						} 
						// if(data.people[j].country != undefined) {
						// 	person += ' | ' + data.people[j].country
						// }
						person += '</h5></div>'
					}
				}
			 	document.getElementById("fellows-wrp").innerHTML+=person;
			 	person = '';
				}


		});

	} else if(year == '2014') {
		document.getElementById("f-2014").setAttribute("class", "a");
	$.getJSON('../../ui/j/json/people.json', function(data) {
				for(var j in data.people) {
					var person = ' '
					var yur = ''
					if(data.people[j].year != undefined) {
						yur  = data.people[j].year.toString()
					}
				
					if(data.people[j].type == "Staff" && (yur.indexOf(year) > -1)) {
						person += '<div class="person">'
						person += '<a href="/about/staff/p/#'+data.people[j].name.replace(/ /g,'')+'">'
						person += '<div class="img-wrpr">'
						person += '<img src="'+data.people[j].pic+'"/></a>'
						person += '</div>'
						person += '<h4>'+data.people[j].name+'</h4>'
						person += '<h5>'

						if(data.people[j].role != undefined) {
							person += data.people[j].role + ' | '
						} 

						if(data.people[j].instrument != undefined) {
							person += data.people[j].instrument
						}
						// if(data.people[j].country != undefined) {
						// 	person += ' | ' + data.people[j].country
						// }
						person += '</h5></div>'
				}
			 	document.getElementById("fellows-wrp").innerHTML+=person;
			 	person = '';
				}
				

				person += '<div class="dv t"></div>'
				
				//COLLABS////
				///
				//
				for(var j in data.people) {

					if(data.people[j].year != undefined) {
						var yur = ''
						if(data.people[j].year != undefined) {
							yur =data.people[j].year.toString()
						}
										

						if(data.people[j].type == "Collaborating Artist" && (yur.indexOf(year) > -1)) {
				 		
						person += '<div class="person">'
						person += '<a href="/about/collaborators/p/#'+data.people[j].name.replace(/ /g,'')+'">'
						person += '<div class="img-wrpr">'
						person += '<img src="'+data.people[j].pic+'"/></a>'
						person += '</div>'
						person += '<h4>'+data.people[j].name+'</h4>'
						person += '<h5>'
						if(data.people[j].type != undefined) {
							person += data.people[j].type + ' | '
						}
						if(data.people[j].instrument != undefined) {
							person += data.people[j].instrument
						} 
						// if(data.people[j].country != undefined) {
						// 	person += ' | ' + data.people[j].country
						// }
						person += '</h5></div>'
					}
				}
			 	document.getElementById("fellows-wrp").innerHTML+=person;
			 	person = '';
				}


		});

	} else if(year == 'core') {
		document.getElementById("f-YR").setAttribute("class", "a");
	$.getJSON('../../ui/j/json/people.json', function(data) {
				for(var j in data.people) {
					var person = ' '
					var yur = ''
					if(data.people[j].year != undefined) {
						yur = data.people[j].year.toString()
					}
				
					if(data.people[j].type == "Staff" && (yur.replace(/ /g,'').indexOf("yearround") > -1 )) {

			 		
						person += '<div class="person">'
						person += '<a href="/about/staff/p/#'+data.people[j].name.replace(/ /g,'')+'">'
						person += '<div class="img-wrpr">'
						person += '<img src="'+data.people[j].pic+'"/></a>'
						person += '</div>'
						person += '<h4>'+data.people[j].name+'</h4>'
						person += '<h5>'

						if(data.people[j].role != undefined) {
							person += data.people[j].role + ' | '
						} 

						if(data.people[j].instrument != undefined) {
							person += data.people[j].instrument
						} 

						// if(data.people[j].country != undefined) {
						// 	person += ' | ' + data.people[j].country
						// }
						person += '</h5></div>'
				}
			 	document.getElementById("fellows-wrp").innerHTML+=person;
			 	person = '';
				}
		});

	}

</script>

	
