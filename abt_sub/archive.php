<script>
// 	function monde() { 
// 		$('.hdr-wrp').addClass('world') 
// 	}
// 	<?php $p = isset($_GET['p']) ? $_GET['p'] : FALSE;
// 		if ($p == 'about'){
// 			echo 'monde()';
// 		}
// 	?>
</script>
<div class="content archive">
	<h2>archive</h2>
	<ul class="filter">
		<li onclick="tTN()" id="f-2014">2014</li> |
		<li onclick="tTR()" id="f-2013">2013</li> |
		<li onclick="tTW()" id="f-2012">2012</li>
	</ul>
	<p class="note">
		OneBeat brings educational workshops, street studios, public art installations, 
		and discussion panels to schools and community centers around the US. Fellows work 
		with local partners in each host city to craft meaningful engagement events for young people, 
		students, and people in the community. Previous OneBeat events, performances and workshops are listed by year.    
	</p>
	<div class="wrp">
		<div id="twl"></div>
		<div id="thr"></div>
	</div>
	<div id="credits" class="credits"></div>
</div>
<script>
	function tTR() {
		var hash = "2013";
		window.location.hash = hash;
		window.location.reload();
	}
	function tTW() {
		var hash = "2012";
		window.location.hash = hash;
		window.location.reload();
	}
	function tTN() {
		var hash = "2014";
		window.location.hash = hash;
		window.location.reload();
	}
	var year = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
	var output 
	var credits
	if(year == '2012') {
		document.getElementById("f-2012").setAttribute("class", "a");
		output = '<h3>Performances</h3> \
			<ul class="Performances" \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.timucua.com">Timucua White House</a> |</li> \
						<li><span>09.22.12</span> |</li> \
						<li>Orlando, FL</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://reduxstudios.org">Redux Art Gallery</a> |</li> \
						<li><span>09.24.12</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.charlestonpourhouse.com">Pour House</a> |</li> \
						<li><span>09.25.12</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.circularchurch.org">Circular Church</a> |</li> \
						<li><span>09.26.12</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://dogtownroadhouse.com">Dogtown Roadhouse</a> |</li> \
						<li><span>09.28.12</span> |</li> \
						<li>Floyd, VA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="https://www.jeffcenter.org">Jefferson Center</a> |</li> \
						<li><span>09.29.12</span> |</li> \
						<li>Roanoke, VA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.busboysandpoets.com">Busboys and Poets</a> |</li> \
						<li><span>10.02.12</span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://atlasarts.org">Atlas Performing Arts Center</a> |</li> \
						<li><span>10.03.12</span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.worldcafelive.com">World Cafe Live</a> |</li> \
						<li><span>10.04.12</span> |</li> \
						<li>Philadelphia, PA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://roulette.org">Roulette</a> |</li> \
						<li><span>10.05.12</span> |</li> \
						<li>Brooklyn, NY</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://bringtolightnyc.org">Autumn Bowl</a> |</li> \
						<li><span>10.06.12</span> |</li> \
						<li>Brooklyn, NY</li> \
					</ul> \
				</li> \
			</ul> \
			<h3>Workshops and Events:</h3> \
			<ul class="workshops"> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://mitchell.ccsdschools.com">Mitchell Elementary School</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.24.12 - 09.25.12</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.spclark.org/">Academy High School</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.24.12 - 09.25.12</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://jrmuseum.cofc.edu">Communications Museum</a> |</li> \
						<li><span>installation</span> |</li> \
						<li><span>09.24.12 - 09.26.12</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.jeffcenter.org/musiclab">Jefferson Center Music Lab</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.29.12 </span> |</li> \
						<li>Roanoke, VA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="">Jackson Middle School</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.29.12</span> |</li> \
						<li>Roanoke, VA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.downtownroanoke.org">Downtown Roanoke</a> |</li> \
						<li><span>street studio</span> |</li> \
						<li><span>09.29.12</span> |</li> \
						<li>Roanoke, VA </li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.howard.edu">Howard University</a> |</li> \
						<li><span>panel discussion</span> |</li> \
						<li><span>10.02.12 </span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.wblinc.org">Words, Beats and Life</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.02.12 - 10.03.12 </span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.sitarartscenter.org">Sitar Arts Center</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.02.12</span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.ellingtonschool.org">Ellington High School</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.03.12</span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.bcamhs.org">Brooklyn Community Arts and Media HS</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.05.12</span> |</li> \
						<li>Brooklyn, NY</li> \
					</ul> \
				</li> \
			</ul>';

		credits = '<h3>Photos:</h3> \
				<h5><a href="http://www.1beatmusic.tumblr.com/">OneBeat 2012 Tumblr Gallery</a></h5> \
				<h5>photography by <a href="">Hannah Devereux</a></h5> \
				<h3>Videos:</h3> \
				<h5><a href="https://www.youtube.com/watch?v=WZCTZm8IZGU&list=PLkLzOTNw7TRTsUS9l0l3WrrCQQplPRdXc">OneBeat 2012 YouTube Playlist</a></h5> \
				<h5>videography by <a href="">Temujin Doran</a></h5> \
				<h3>Blog:</h3> \
				<h5><a href="http://foundsoundnation.org/curations/onebeat-2012/">OneBeat 2012 Tumblr Gallery</a></h5> \
				<h5>curated by <a href="http://drzzl.com/">Adriel Luis</a></h5>';

		document.getElementById("twl").innerHTML+=output;
		document.getElementById("credits").innerHTML+=credits;

	} else if (year == '2013') {
		document.getElementById("f-2013").setAttribute("class", "a");
		output = '<h3>Performances</h3> \
			<ul class="Performances" \
				<li class="venue"> \
					<ul> \
						<li><a href="http://artsondouglas.net">Arts on Douglas Gallery</a> |</li> \
						<li><span>09.18.13</span> |</li> \
						<li>New Smyrna Beach, FL</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.timucua.com">Timucua White House</a> |</li> \
						<li><span>09.20.13</span> |</li> \
						<li>Orlando, FL</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.atlanticcenterforthearts.org">Atlantic Center for the Arts</a> |</li> \
						<li><span>09.21.13 </span> |</li> \
						<li>New Smyrna Beach, FL</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://reduxstudios.org">Redux Art Gallery</a> |</li> \
						<li><span>09.23.13</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.charlestonpourhouse.com">Pour House</a> |</li> \
						<li><span>09.24.13 </span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.circularchurch.org">Circular Church</a> |</li> \
						<li><span>09.25.13</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.moreheadplanetarium.org">UNC Planetarium</a> |</li> \
						<li><span>09.27.13</span> |</li> \
						<li>Chapel Hill, NC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://motorcomusic.com">Motorco</a> |</li> \
						<li><span>09.28.13 </span> |</li> \
						<li>Durham, NC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.kennedy-center.org/programs/millennium/">Millenium Stage at Kennedy Center</a> |</li> \
						<li><span>10.01.13</span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.busboysandpoets.com">Busboys and Poets</a> |</li> \
						<li><span>10.01.13</span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://bricartsmedia.org">BRIC Arts</a> |</li> \
						<li><span>10.03.13</span> |</li> \
						<li>Brooklyn, NY</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://irondale.org">Irondale Center</a> |</li> \
						<li><span>10.05.13</span> |</li> \
						<li>Brooklyn, NY</li> \
					</ul> \
				</li> \
			</ul> \
			<h3>Workshops and Events:</h3> \
			<ul class="workshops"> \
				<li class="venue"> \
					<ul> \
						<li><a href="">Academy High School</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.23.13 - 09.25.13</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://girlsrockcharleston.org">Girls Rock</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.23.13 - 09.24.13</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.spclark.org/">Clark Academy</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.23.13 - 09.24.13</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://murraylasaine.ccsdschools.com">Murray-Lasaine School</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.23.13 - 09.24.13</span> |</li> \
						<li>Charleston, SC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.charlestonparksconservancy.org/our_parks/view_park/marion_square/">Marion Square</a> |</li> \
						<li><span>street studio</span> |</li> \
						<li><span>09.23.13 - 09.25.13 </span> |</li> \
						<li>Charleston, SC </li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.beatmakinglab.com">Beatmaking Lab</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.26.13 - 09.27.13</span> |</li> \
						<li>Chapel Hill, NC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.kidznotes.org">Kidznotes</a> |</li> \
						<li><span>workshop and concert</span> |</li> \
						<li><span>09.27.13 - 09.28.13</span> |</li> \
						<li>Durham, NC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.durhamarts.org">Durham Arts Center</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.27.13</span> |</li> \
						<li>Durham, NC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="">Carrboro Arts Center</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.27.13</span> |</li> \
						<li>Carrboro, NC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.artscenterlive.org">Carrboro High School </a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.27.13</span> |</li> \
						<li>Carrboro, NC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://sacrificialpoets.org">Sacrificial Poets</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>09.27.13</span> |</li> \
						<li>Brooklyn, NY</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.american.edu">American University</a> |</li> \
						<li><span>panel discussion</span> |</li> \
						<li><span>10.01.13</span> |</li> \
						<li>Washington, DC</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://schools.nyc.gov/SchoolPortals/05/M285/default.htm">Renaissance HS</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.03.13</span> |</li> \
						<li>Harlem, NY</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.bcamhs.org">Brooklyn Community Arts and Media HS</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.03.13</span> |</li> \
						<li>Brooklyn, NY</li> \
					</ul> \
				</li> \
			</ul>'; 

		credits = '<h3>Photos:</h3> \
			<h5><a href="http://foundsoundnation.org/curations/onebeat-2013-gallery/">OneBeat 2013 Tumblr Gallery</a></h5> \
			<h5>photography by <a href="http://www.hannahdevereux.com">Hannah Devereux</a></h5> \
			<h3>Videos:</h3> \
			<h5><a href="https://www.youtube.com/watch?v=as4yq5-14b4&list=PLkLzOTNw7TRQ28XnTjOGYpT8TtSlE5P9B">OneBeat 2013 YouTube Playlist</a></h5> \
			<h5>videography by <a href="http://studiocanoe.com">Temujin Doran</a></h5> \
			<h3>Blog:</h3> \
			<h5><a href="http://foundsoundnation.org/curations/onebeat-2012/">OneBeat 2013 Blog</a></h5> \
			<h5>curated by <a href="http://tjryan.com">TJ Ryan</a></h5>';

		document.getElementById("thr").innerHTML+=output;
		document.getElementById("credits").innerHTML+=credits;
	
	} else if (year == '2014') {
		document.getElementById("f-2014").setAttribute("class", "a");
		output = '<h3>Performances</h3> \
			<ul class="Performances" \
				<li class="venue"> \
					<ul> \
						<li><a href="http://publicsf.com">Public Works</a> |</li> \
						<li><span>10.15.14</span> |</li> \
						<li>San Francisco, CA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://montalvoarts.org/performance/">Montalvo Arts Center</a> |</li> \
						<li><span>10.19.14</span> |</li> \
						<li>Saratoga, CA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.sonos-studio.com/Home/About">Sonos Studio</a> |</li> \
						<li><span>10.22.14</span> |</li> \
						<li>Los Angeles, CA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.mariachi-plaza.com/">Mariachi Plaza</a> |</li> \
						<li><span>10.24.14</span> |</li> \
						<li>Los Angeles, CA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://artsharela.org">Art Share L.A.</a> |</li> \
						<li><span>10.25.14</span> |</li> \
						<li>Los Angeles, CA</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="https://arcosanti.org">Arcosanti</a> |</li> \
						<li><span>10.28.14</span> |</li> \
						<li>Cordes Lakes, AZ</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://sisterthebar.com">Sister Bar</a> |</li> \
						<li><span>10.30.14</span> |</li> \
						<li>Albuquerque, NM</li> \
					</ul> \
				</li> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://www.railyardsmarket.org/">Albuquerque Railyards</a> |</li> \
						<li><span>11.01.14</span> |</li> \
						<li>Albuquerque, NM</li> \
					</ul> \
				</li> \
			</ul> \
			<h3>Workshops and Events:</h3> \
			<ul class="workshops"> \
				<li class="venue"> \
					<ul> \
						<li><a href="http://jlhsforensics.org/about">James Logan High School</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.13.14</span> |</li> \
						<li>Saratoga, CA</li> \
					</ul> \
					<li class="venue"> \
					<ul> \
						<li><a href="http://montalvoarts.org/events/pass2014_onebeat/">Local K-8 School Visit</a> |</li> \
						<li><span>performance</span> |</li> \
						<li><span>10.15.14</span> |</li> \
						<li>Saratoga, CA</li> \
					</ul> \
					<li class="venue"> \
					<ul> \
						<li><a href="http://mendez-lausd-ca.schoolloop.com/cms/page_view?d=x&piid=&vpid=1249305211413">Mendez Learning Academy</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.22.14 - 10.24.14</span> |</li> \
						<li>Los Angeles, CA</li> \
					</ul> \
					<li class="venue"> \
					<ul> \
						<li><a href="http://www.themariachiconservatory.com/">Mariachi Conservatory</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.23.14</span> |</li> \
						<li>Los Angeles, CA</li> \
					</ul> \
					<li class="venue"> \
					<ul> \
						<li><a href="http://www.boyleheightsyouthorchestra.org/">Boyle Heights Community Youth Orchestra</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.23.14</span> |</li> \
						<li>Los Angeles, CA</li> \
					</ul> \
					<li class="venue"> \
					<ul> \
						<li><a href="http://www.usc.edu/">Panel Discussion at USC</a> |</li> \
						<li><span>panel</span> |</li> \
						<li><span>10.23.14</span> |</li> \
						<li>Los Angeles, CA</li> \
					</ul> \
					<li class="venue"> \
					<ul> \
						<li><a href="http://leadershiphsn.org/health/">Health Leadership High School</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>10.31.14</span> |</li> \
						<li>Albuquerque, NM</li> \
					</ul> \
					<li class="venue"> \
					<ul> \
						<li><a href="http://www.nationalhispaniccenter.org/">National Hispanic Cultural Center</a> |</li> \
						<li><span>workshop</span> |</li> \
						<li><span>11.01.14</span> |</li> \
						<li>Albuquerque, NM</li> \
					</ul> \
					<li class="venue"> \
					<ul> \
						<li><a href="http://www.workingclassroom.org/">Working Classroom</a> |</li> \
						<li><span>workshop & performance</span> |</li> \
						<li><span>11.02.14</span> |</li> \
						<li>Albuquerque, NM</li> \
					</ul> \
			</ul>'; 

		credits = '<h3>Photos:</h3> \
			<h5><a href="http://1beat.org/festival/?p=festival#photoblog">OneBeat 2014 Photo Blog</a></h5> \
			<h5>photography by <a href="http://www.hannahdevereux.co.uk">Hannah Devereux</a></h5> \
			<h3>Videos:</h3> \
			<h5><a href="http://1beat.org/festival/?p=festival#videos">OneBeat 2014 YouTube Playlist</a></h5> \
			<h5>videography by <a href="http://studiocanoe.com">Temujin Doran</a></h5>'

		document.getElementById("thr").innerHTML+=output;
		document.getElementById("credits").innerHTML+=credits;
	
	}

	

</script>
