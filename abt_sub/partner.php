<script>
	// function monde() { 
	// 	$('.hdr-wrp').addClass('world') 
	// }
	// <?php $p = isset($_GET['p']) ? $_GET['p'] : FALSE;
	// 	if ($p == 'about'){
	// 		echo 'monde()';
	// 	}
	// ?>
</script>
<div class="content partner">
	<h2>partners</h2>
	<ul class="filter">
		<li onclick="partners()" id="f-partners">Partners</li> |
		<li onclick="supporters()" id="f-supporters">Supporters and Sponsors</li> 
	</ul>
	<div class="wrp">
		<div id="sponsor"></div>
		<div id="partner"></div>
		<div id="supporter"></div>
	</div>
</div>
<script>
	function partners() {
		var hash = "partners";
		window.location.hash = hash;
		window.location.reload();
	}
	function supporters() {
		var hash = "supporters-sponsors";
		window.location.hash = hash;
		window.location.reload();
	}
	var type = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
	var output 
	if(type == 'partners') {
		document.getElementById("f-partners").setAttribute("class", "a");
		output = '<div class="info"> \
					<h3>A Program Of:<a href="http://exchanges.state.gov/"> ECA</a></h3> \
					<p>OneBeat is an initiative of the U.S. Department of State'+"'"+'s Bureau of Educational and Cultural Affairs. \
					The Bureau supports U.S. foreign policy by fostering mutual understanding between the people of the United States \
					and the people of other countries through academic, cultural, sports, and professional exchanges that engage women, \
					youth, underserved communities, educators, artists, athletes, and rising leaders in the U.S. and more than 160 countries.</p> \
					</div> \
					<div class="info"> \
						<h3>Produced by:<a href="http://foundsoundnation.org/curations/"> Found Sound Nation</a></h3> \
						<p>OneBeat is produced by Found Sound Nation, an eclectic group of musicians, producers, and \
						artists who have a strong sense of social engagement. Working with people across the globe, \
						from schools to prisons, from young to old, and partnering with local youth, social organizations, \
						music festivals, and artists across all disciplines, Found Sound Nation uses the expressive \
						power of music collaboration to unlock the creative potential of youth, and build bridges between \
						communities separated by cultures, economic disparities, and geography. Found Sound Nation \
						is the social engagement wing of Bang on a Can, the groundbreaking organization for new music.</p> \
					</div> \
					<div class="info"> \
						<h3>Administered by:<a href="http://bangonacan.org/"> Bang on a Can</a></h3> \
						<p>Bang on a Can is dedicated to making music new. Since its first Marathon concert in 1987, \
						Bang on a Can has been creating an international community dedicated to innovative music, wherever \
						it is found. With adventurous programs, it commissions new composers, performs, presents, and records \
						new work, develops new audiences, and educates the musicians of the future. Bang on a Can is building a \
						world in which powerful new musical ideas flow freely across all genres and borders. </p> \
					</div> \
					<div class="info"> \
						<h3>Additional Major Support Provided by:<a href="http://www.rsclark.org/"> Robert Sterling Clark Foundation</a></h3> \
						<p>The Robert Sterling Clark Foundation was incorporated in 1952 and has since provided financial \
						assistance to a wide variety of organizations. At present, they are concentrating their resources in the \
						following three fields: Promoting International Arts Engagement, Protecting Reproductive Rights, \
						Improving the Performance of Public Institutions in New York. The objective of the Foundation'+"'"+'s \
						international program is to promote global understanding through the arts. The Foundation has \
						initial plans to support international engagement involving U.S. visual and performing arts organizations \
						and their counterparts primarily in Africa, Latin America and the Middle East.</p> \
					</div> \
				</div>';

		document.getElementById("partner").innerHTML+=output;


	} else if(type == 'supporters-sponsors') {
		document.getElementById("f-supporters").setAttribute("class", "a");

		sponsor = '<div class="sponsors"> \
		<div class="sml small cf"> \
			<div class="one cf"> \
				<a href="http://montalvoarts.org/" class="logo-p mont"></a> \
				<a href="http://516arts.org/" class="logo-p fos"></a> \
				<a href="https://arcosanti.org/" class="logo-p arco"></a> \
				<a href="http://artsharela.org/" class="logo-p art-shr"></a> \
			</div> \
			<div class="two cf"> \
				<a href="http://ghostranch.org/" class="logo-p ghost-ranch"></a> \
				<a href="https://www.nhccfoundation.org/" class="logo-p nhccf"></a> \
				<a href="" class="logo-p yards"></a> \
				<a href="https://www.ableton.com/" class="logo-p abl"></a> \
			</div> \
			<div class="three cf"> \
				<a href="http://www.pelusomicrophonelab.com/" class="logo-p pel"></a> \
				<a href="http://worldup.org/" class="logo-p world-up"></a> \
				<a href="http://to.be/" class="logo-p tobe"></a> \
				<a href="http://www.glyphtech.com/" class="logo-p glyph"></a> \
				<a href="http://www.daddario.com/DaddarioSplash.Page?ActiveID=1740" class="logo-p dard"></a> \
				<a href="http://www.dubspot.com/" class="logo-p dspot"></a> \
			</div> \
		</div> \
	</div>';


		document.getElementById("sponsor").innerHTML+=sponsor;


		output = '<h2>2013</h2> \
		<div class="info"> \
			<h3>Arts on Douglas Gallery  |  New Smyrna Beach, FL</h3> \
			<a href="http://artsondouglas.net">http://artsondouglas.net</a> \
			<p>Arts on Douglas is a contemporary art gallery and event space in New Smyrna Beach, \
			dedicated to promoting the work of local artists. </p> \
		</div> \
		<div class="info"> \
			<h3>Timucua White House  |  Orlando, FL</h3> \
			<a href="http://www.timucua.com">http://www.timucua.com</a> \
			<p>Timucua White House is a renowned Orlando non-profit \
			venue and vital proponent of jazz and avant music.</p> \
		</div> \
		<div class="info"> \
			<h3>Atlantic Center for the Arts   |  New Smyrna Beach, FL</h3> \
			<a href="http://www.atlanticcenterforthearts.org">http://www.atlanticcenterforthearts.org</a> \
			<p>Founded in 1977 by artist Doris Leeper, Atlantic Center for the Artists \
			is an internationally recognized multi-disciplinary artist residency facility.</p> \
		</div> \
		<div class="info"> \
			<h3>Redux Art Gallery  |  Charleston, SC</h3> \
			<a href="http://reduxstudios.org">http://reduxstudios.org</a> \
			<p>Redux Contemporary Art Center is a nonprofit organization committed to \
			fostering creativity and the cultivation of contemporary art through diverse exhibitions, \
			subsidized studio space for visual artists and meaningful education programs.</p> \
		</div> \
		<div class="info"> \
			<h3>Pour House |  Charleston, SC</h3> \
			<a href="http://www.charlestonpourhouse.com">http://www.charlestonpourhouse.com</a> \
			<p>The Pour House is a concert hall and tavern showcasing top touring acts and local favorites.</p> \
		</div> \
		<div class="info"> \
			<h3>Circular Church   |  Charleston, SC</h3> \
			<a href="http://www.circularchurch.org">http://www.circularchurch.org</a> \
			<p>Founded in 1681, Circular Congregational Church is a landmark building \
			and concert space in downtown Charleston. </p> \
		</div> \
		<div class="info"> \
			<h3>UNC Planetarium  |  Chapel Hill, NC</h3> \
			<a href="http://www.moreheadplanetarium.org">http://www.moreheadplanetarium.org</a> \
			<p>The UNC Morehead Planetarium and Science Center informs and inspires the \
			public about the foundations and frontiers of scientific discovery. \
			Through innovative educational experiences, Morehead engages the public and the \
			University community in a forum for interpreting contemporary science.</p> \
		</div> \
		<div class="info"> \
			<h3>Motorco |  Durham, NC</h3> \
			<a href="http://motorcomusic.com">http://motorcomusic.com</a> \
			<p>Motorco Music Hall is an entertainment and events space in Durham, \
			North Carolina providing eclectic all ages programming.</p> \
		</div> \
		<div class="info"> \
			<h3>Millenium Stage at Kennedy Center  |  Washington, DC</h3> \
			<a href="http://www.kennedy-center.org/programs/millennium/">http://www.kennedy-center.org/</a> \
			<p>Since opening in 1971, the Kennedy Center has continued its \
			efforts to fulfill his vision-presenting the greatest performers and performances \
			from across America and around the world, nurturing new works and young artists, \
			and serving the nation as a leader in arts education.</p> \
		</div> \
		<div class="info"> \
			<h3>Busboys and Poets |  Washington, DC</h3> \
			<a href="http://www.busboysandpoets.com">http://www.busboysandpoets.com</a> \
			<p>Busboys and Poets is an activist bookstore, lounge, restaurant and theater \
			with two locations in Washington, D.C.</p>	\
		</div> \
		<div class="info"> \
			<h3>BRIC Arts |  Brooklyn, NY</h3> \
			<a href="http://bricartsmedia.org">http://bricartsmedia.org</a> \
			<p>BRIC is a presenter for contemporary art, performing arts and community \
			media programs that reflect Brooklyns creativity and diversity. BRIC also \
			provides resources to launch, nurture and showcase artists and media makers, \
			as well as education and other public programs to people of all ages.</p> \
		</div> \
		<div class="info"> \
			<h3>Irondale Center  |  Brooklyn, NY</h3> \
			<a href="http://irondale.org">http://irondale.org</a> \
			<p>The Irondale Center is a Fort Greene-based theatre company \
			and venue presenting cutting edge performing arts.</p> \
		</div> \
		<div class="info"> \
			<h3>Girls Rock Charleston  |  Charleston, SC</h3> \
			<a href="http://girlsrockcharleston.org">http://girlsrockcharleston.org</a> \
			<p>Girls Rock Charleston empowers girls and transgender youth through music \
			education, DIY media, and creative collaboration.</p> \
		</div> \
		<div class="info"> \
			<h3>Murray-Lasaine School |  Charleston, SC</h3> \
			<a href="http://murraylasaine.ccsdschools.com">http://murraylasaine.ccsdschools.com</a> \
			<p>Murray-LaSaine is a “family centered” neighborhood school with a big heart, outstanding \
			academic programs and a strong vision for the future.</p> \
		</div> \
		<div class="info"> \
			<h3>Marion Square  |  Charleston, SC</h3> \
			<a href="http://www.charlestonparksconservancy.org/our_parks/view_park/marion_square/">http://www.charlestonparksconservancy.org/</a> \
		</div> \
		<div class="info"> \
			<h3>Beatmaking Lab |  Chapel Hill, NC</h3> \
			<a href="http://www.beatmakinglab.com">http://www.beatmakinglab.com</a> \
			<p>Beat Making Lab is an electronic music studio promoting cultural \
			exchange and innovative collaboration with a social/entrepreneurial impact.</p> \
		</div> \
		<div class="info"> \
			<h3>Kidznotes  |  Durham, NC</h3> \
			<a href="http://www.kidznotes.org">http://www.kidznotes.org</a> \
			<p>Kidznotes is an El-Sistema inspired nonprofit that changes the life \
			trajectory of underserved K-12 students through orchestral training.</p> \
		</div> \
		<div class="info"> \
			<h3>Durham Arts Council  |   Durham, NC</h3> \
			<a href="http://www.durhamarts.org">http://www.durhamarts.org</a> \
			<p>Durham Arts Council is a private nonprofit dedicated to supporting \
			the arts in Durham and the Triangle Region in North Carolina.</p> \
		</div> \
		<div class="info"> \
			<h3>Carrboro ArtsCenter  |   Carrboro, NC</h3> \
			<a href="http://www.artscenterlive.org">http://www.artscenterlive.org</a> \
			<p>Carrboro ArtsCenter is a dynamic arts center that nourishes creativity and community \
			through education, performance, exhibitions and public programming.</p> \
		</div> \
		<div class="info"> \
			<h3>Carrboro High School  |  Carrboro, NC</h3> \
			<a href="http://chs.chccs.k12.nc.us">http://chs.chccs.k12.nc.us</a> \
			<p>Founded in 2007, Carrboro high school has been recognized for its diversity, \
			academic rigor and 21st-century state-of-the-art facilities.</p> \
		</div> \
		<div class="info"> \
			<h3>Sacrificial Poets  |  Carrboro, NC</h3> \
			<a href="http://sacrificialpoets.org"></a> \
			<p>Sacrificial Poets is an award-winning spoken word poetry arts education organization \
			that serves elementary, middle, high school and college-aged youth across North Carolina.</p> \
		</div> \
		<div class="info"> \
			<h3>American University  |  Washington, DC</h3> \
			<a href="http://www.american.edu">http://www.american.edu</a> \
			<p>American University is a leader among Washington D.C. universities \
			in global education, recognized for promoting cultural diplomacy and  \
			international understanding.</p> \
		</div> \
		<div class="info"> \
			<h3>Renaissance High School  |   Harlem, NY</h3> \
			<a href="http://schools.nyc.gov/SchoolPortals/05/M285/default.htm">http://schools.nyc.gov/</a> \
			<p>Harlem Renaissance High School allows students who have not made the most of their ninth grade \
			year at other schools to refresh and catch up to their peers, while benefiting from a full range of arts and enrichment programs.</p> \
		</div> \
		<div class="info"> \
			<h3>Brooklyn Community Arts and Media High School  |  Brooklyn, NY</h3> \
			<a href="http://www.bcamhs.org">http://www.bcamhs.org</a> \
			<p>Brooklyn Community Arts & Media High School (BCAM) is an art, media, \
			and technology-based school which promotes community collaboration and meaningful social change.</p> \
		</div> \
		<h2>2012</h2> \
		<div class="info"> \
			<h3>Arts on Douglas Gallery | New Smyrna Beach, FL</h3> \
			<a href="http://artsondouglas.net">http://artsondouglas.net</a> \
			<p>Arts on Douglas is a contemporary art gallery and event space in New Smyrna Beach, \
			dedicated to nurturing the careers of local and emerging artists. </p> \
		</div> \
		<div class="info"> \
			<h3>Timucua White House  |  Orlando, FL</h3> \
			<a href="http://www.timucua.com">http://www.timucua.com</a> \
			<p>Timucua White House is an intimate non-profit venue, serving as a vital proponent of jazz \
			and avant music in Orlando. </p> \
		</div> \
		<div class="info"> \
			<h3>Atlantic Center for the Arts  |  New Smyrna Beach, FL</h3> \
			<a href="http://www.atlanticcenterforthearts.org">http://www.atlanticcenterforthearts.org</a> \
			<p>Founded in 1977 by artist Doris Leeper, Atlantic Center for the Artists \
			is an internationally renowned multi-disciplinary artist residency facility.</p> \
		</div> \
		<div class="info"> \
			<h3>Redux Contemporary Art Center  |  Charleston, SC</h3> \
			<a href="http://reduxstudios.org">http://reduxstudios.org</a> \
			<p>Redux Contemporary Art Center is a nonprofit organization committed to fostering \
			creativity and the cultivation of contemporary art through diverse exhibitions, \
			subsidized studio space for artists and meaningful education programs.</p> \
		</div> \
		<div class="info"> \
			<h3>Pour House |  Charleston, SC</h3> \
			<a href="http://www.charlestonpourhouse.com">http://www.charlestonpourhouse.com</a> \
			<p>The Pour House is one of Charlestons top concert venues, \
			showcasing top touring acts and curated local favorites.</p> \
		</div> \
		<div class="info"> \
			<h3>Circular Congregational Church  |  Charleston, SC</h3> \
			<a href="http://www.circularchurch.org">http://www.circularchurch.org</a> \
			<p>Founded in 1681, Circular Congregational Church is a stunning landmark and concert \
			space located in downtown Charleston.</p> \
		</div> \
		<div class="info"> \
			<h3>Dogtown Roadhouse |  Floyd, VA</h3> \
			<a href="http://dogtownroadhouse.com">http://dogtownroadhouse.com</a> \
			<p>Floyds best kept secret is a stomping ground for regional and national touring acts in Appalachia.</p> \
		</div> \
		<div class="info"> \
			<h3>Jefferson Center |  Roanoke, VA</h3> \
			<a href="https://www.jeffcenter.org">https://www.jeffcenter.org</a> \
			<p>The Jefferson Center is a nonprofit arts and community center educating students \
			and inspiring audiences throughout Southwest Virginia.</p> \
		</div> \
		<div class="info"> \
			<h3>Busboys and Poets  |  Washington, DC</h3> \
			<a href="http://www.busboysandpoets.com"></a> \
			<p>Busboys and Poets is an activist bookstore, lounge, \
			restaurant and theater home to regular screenings, \
			discussions, readings and performances.</p> \
		</div> \
		<div class="info"> \
			<h3>Atlas Performing Arts Center   |  Washington, DC</h3> \
			<a href="http://atlasarts.org">http://atlasarts.org</a> \
			<p>Atlas Performing Arts Center is a multiple-space, state of the art \
			facility for performing arts and film in the heart of the Atlas District.</p> \
		</div> \
		<div class="info"> \
			<h3>World Cafe Live  |  Philadelphia, PA</h3> \
			<a href="http://www.worldcafelive.com">http://www.worldcafelive.com</a> \
			<p>World Cafe Live is a three-tiered music venue and restaurant, \
			operated conjunction with the public radio syndicated show.</p> \
		</div> \
		<div class="info"> \
			<h3>Roulette Intermedium |  Brooklyn, NY</h3> \
			<a href="http://roulette.org">http://roulette.org</a> \
			<p>Founded in 1978, Brooklyns Roulette Intermedium is one of the most creative and \
			prolific venues for experimental art in the United States.</p> \
		</div> \
		<div class="info"> \
			<h3>Autumn Bowl  |  Brooklyn, NY</h3> \
			<a href="http://bringtolightnyc.org">http://bringtolightnyc.org</a> \
			<p>OneBeat partnered with Bring to Light: Nuit Blanche New York, a free nighttime \
			contemporary art event, to transform a Greenpoint warehouse into an immersive installation \
			and performance space.</p> \
		</div> \
		<div class="info"> \
			<h3>Mitchell Elementary School  |  Charleston, SC</h3> \
			<a href="http://mitchell.ccsdschools.com">http://mitchell.ccsdschools.com</a> \
		</div> \
		<div class="info"> \
			<h3>Clark Academy High School  |  Charleston, SC</h3> \
		</div> \
		<div class="info"> \
			<h3>John Rivers Communications Museum |  Charleston, SC</h3> \
			<a href="http://jrmuseum.cofc.edu">http://jrmuseum.cofc.edu</a> \
			<p>The John Rivers Communication Museum is dedicated to cataloguing the history \
			and entertainment of the communications and broadcasting fields.</p> \
		</div> \
		<div class="info"> \
			<h3>Jefferson Center Music Lab |   Roanoke, VA</h3> \
			<a href="http://www.jeffcenter.org/musiclab">http://www.jeffcenter.org/musiclab</a> \
			<p>Jefferson Center is a nonprofit arts and community center educating \
			students & inspiring audiences throughout Southwest Virginia. </p> \
		</div> \
		<div class="info"> \
			<h3>Downtown Roanoke ||  Roanoke, VA</h3> \
			<a href="http://www.downtownroanoke.org">http://www.downtownroanoke.org</a> \
		</div> \
		<div class="info"> \
			<h3>Howard University   |  Washington, DC</h3> \
			<a href="http://www.howard.edu">http://www.howard.edu</a> \
			<p>Founded in 1867, Howard University is a renowned private university with a strong \
			history of promoting activism and free speech.</p> \
		</div> \
		<div class="info"> \
			<h3>Words, Beats and Life |  Washington, DC</h3> \
			<a href="http://www.wblinc.org">http://www.wblinc.org</a> \
			<p>Words Beats & Life is nonprofit organization that engages youth \
			and communities through holistic Hip-Hop programming.</p> \
		</div> \
		<div class="info"> \
			<h3>Sitar Arts Center  |  Washington, DC</h3> \
			<a href="http://www.sitarartscenter.org">http://www.sitarartscenter.org</a> \
			<p>Sitar Arts Center advances the critical life skills of underserved \
			children and youth and prepares them for achievement in the 21st century \
			through visual, performing, and digital arts education in a nurturing community.</p> \
		</div> \
		<div class="info"> \
			<h3>Duke Ellington High School  |  Washington, DC</h3> \
			<a href="http://www.ellingtonschool.org">http://www.ellingtonschool.org</a> \
			<p>Duke Ellington School for the Arts is an award-winning secondary school inspired \
			by Duke Ellington, committed to "learning in a disciplined yet improvisational manner."</p> \
		</div> \
		<div class="info"> \
			<h3>School Without Walls  |  Washington, DC</h3> \
			<a href="http://www.swwhs.org/?option=com_contact&view=contact&id=1&Itemid=19">http://www.swwhs.org/</a> \
			<p>The School Without Walls is an alternative model high school in \
			partnership with George Washington University.</p> \
		</div> \
		<div class="info"> \
			<h3>Brooklyn Community Arts and Media HS   |  Brooklyn, NY</h3> \
			<a href="http://www.bcamhs.org">http://www.bcamhs.org</a> \
			<p>Brooklyn Community Arts & Media High School (BCAM) art, media, and \
			technology-based school which promotes community collaboration and meaningful social change.</p> \
		</div>';
		document.getElementById("supporter").innerHTML+=output;
	} 
	
		
	
</script>