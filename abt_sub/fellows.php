<script>
	// function monde() { 
	// 	$('.hdr-wrp').addClass('world') 
	// }
	// <?php $p = isset($_GET['p']) ? $_GET['p'] : FALSE;
	// 	if ($p == 'about'){
	// 		echo 'monde()';
	// 	}
	// ?>
</script>
<div class="content fellows" >
	<h2>Fellows</h2>
	<h3 class="maap"><a href="/community/map">Map</a></h3>
	<ul class="filter">
		<li onclick="tTN()" id="f-2014">2014</li> |
		<li onclick="tTR()" id="f-2013">2013</li> |
		<li onclick="tTW()" id="f-2012">2012</li>

	</ul>
	<div class="wrp">
		<div id="twl"></div>
		<div id="thr"></div>
	</div>
	<div class="people cf" id="fellows-wrp"></div>
</div>

<script>
	function tTR() {
		var hash = "2013";
		window.location.hash = hash;
		window.location.reload();
	}
	function tTN() {
		var hash = "2014";
		window.location.hash = hash;
		window.location.reload();
	}
	function tTW() {
		var hash = "2012";
		window.location.hash = hash;
		window.location.reload();
	}
	var year = document.URL.substring(document.URL.lastIndexOf("#")+1,document.URL.length);
	var output 
	if(year == '2012') {
		document.getElementById("f-2012").setAttribute("class", "a");
			$.getJSON('../../ui/j/json/people.json', function(data) {

				for(var j in data.people) {
					if(data.people[j].year != undefined) {
						var person = ' '
						var yur = data.people[j].year.toString()
					
						if(data.people[j].type == "Fellow" && (yur.indexOf(year) > -1)) {
				 		
						person += '<div class="person">'
						person += '<a href="/about/fellows/p/#'+data.people[j].name.replace(/ /g,'')+'">'
						person += '<div class="img-wrpr">'
						person += '<img src="'+data.people[j].pic+'"/></a>'
						person += '</div>'
						person += '<h4>'+data.people[j].name+'</h4>'
						person += '<h5>'
						if(data.people[j].instrument != undefined) {
							person += data.people[j].instrument
						} 
						if(data.people[j].disc != undefined) {
							person += ', ' + data.people[j].disc 
						}
						if(data.people[j].country != undefined) {
							person += ' | ' + data.people[j].country
						}
						
						person += '</h5></div>'
					}
				 	document.getElementById("fellows-wrp").innerHTML+=person;
				 	person = '';
				 }
				}
		});

	}else if(year == '2013') {
		document.getElementById("f-2013").setAttribute("class", "a");
		$.getJSON('../../ui/j/json/people.json', function(data) {
			for(var j in data.people) {
				var person = ''
				if(data.people[j].year != undefined) {
					var yur = data.people[j].year.toString()
					console.log(yur)
					
					console.log("test:  " + yur.indexOf(year))
					if(data.people[j].type == "Fellow" && (yur.indexOf(year) > -1)) {
						console.log("hi")
						person += '<div class="person">'
						person += '<a href="/about/fellows/p/#'+data.people[j].name.replace(/ /g,'')+'">'
						person += '<div class="img-wrpr">'
						person += '<img src="'+data.people[j].pic+'"/></a>'
						person += '</div>'
						person += '<h4>'+data.people[j].name+'</h4>'
						person += '<h5>'
						if(data.people[j].instrument != undefined) {
							person += data.people[j].instrument
						} 
						if(data.people[j].disc != undefined) {
							person += ', ' + data.people[j].disc 
						}
						if(data.people[j].country != undefined) {
							person += ' | ' + data.people[j].country
						}
						person += '</h5></div>'
					}
					document.getElementById("fellows-wrp").innerHTML+=person;
					person = '';
				}
			}
		});
	} else if(year == '2014') {
		document.getElementById("f-2014").setAttribute("class", "a");
		$.getJSON('../../ui/j/json/people.json', function(data) {
		
			for(var j in data.people) {
				var person = ''
				if(data.people[j].year != undefined) {
					var yur = data.people[j].year.toString()
					if(data.people[j].type == "Fellow" && (yur.indexOf(year) > -1)) {
						console.log("hi")
						person += '<div class="person">'
						person += '<a href="p/#'+data.people[j].name.replace(/ /g,'')+'">'
						person += '<div class="img-wrpr">'
						person += '<img src="'+data.people[j].pic+'"/></a>'
						person += '</div>'
						person += '<h4>'+data.people[j].name+'</h4>'
						person += '<h5>'
						if(data.people[j].instrument != undefined) {
							person += data.people[j].instrument
						} 
						if(data.people[j].disc != undefined) {
							person += ', ' + data.people[j].disc 
						}
						if(data.people[j].country != undefined) {
							person += ' | ' + data.people[j].country
						}
						person += '</h5></div>'
					}
					document.getElementById("fellows-wrp").innerHTML+=person;
					person = '';
				}
			}
		});  
	}

</script>

	
